import subprocess
import time
import sys
import gzip
import bz2
import re
import os

from helper import CSV_PATH, PLOT_PATH
from helper import PCAP1, PCAP2
from helper import FLOW_FILE_EXTENSION, BUFFER_FILE_EXTENSION, COMPRESSION_EXTENSIONS, COMPRESSION_METHODS


MAX_HOST_NUMBER = 256**2

DEFAULT_BANDWIDTH = 10
DEFAULT_BUFFER_SIZE = 100

colors = {
    'red': '[1;31;40m',
    'green': '[1;32;40m',
    'yellow': '[1;33;40m',
}


def print_error(line):
    print(colorize(line, 'red'))


def print_warning(line):
    print(colorize(line, 'yellow'))


def print_success(line):
    print(colorize(line, 'green'))


def colorize(string, color=None):
    if color not in colors.keys():
        return string
    return '\x1b{color}{string}\x1b[0m'.format(color=colors[color], string=string)


def get_git_revision_hash():
    pass
    # try:
    #     return subprocess.check_output(['git', 'rev-parse', 'HEAD'], stderr=subprocess.PIPE).rstrip()
    # except subprocess.CalledProcessError as e:
    #     print_error(e)
    #     return 'unknown'


def get_host_version():
    try:
        return subprocess.check_output(['uname', '-ovr'], stderr=subprocess.PIPE).rstrip()
    except subprocess.CalledProcessError as e:
        print_error(e)
        return 'unknown'


def get_available_algorithms():
    try:
        return subprocess.check_output(['sysctl net.ipv4.tcp_available_congestion_control '
                                        '| sed -ne "s/[^=]* = \(.*\)/\\1/p"'])
    except subprocess.CalledProcessError as e:
        print_error('Cannot retrieve available congestion control algorithms.')
        print_error(e)
        return ''


def check_tools():
    missing_tools = []
    tools = {
        'netcat': 'netcat',
        'cpunetlog': 'cpunetlog',
        'tcplog': 'tcplog'
    }

    for package, tool in tools.items():
        if not check_tool(tool):
            missing_tools.append(package)

    if len(missing_tools) > 0:
        print_error('Missing tools. Please install:')
        print_error(' '.join(missing_tools))

    return len(missing_tools)


def check_tool(tool):
    try:
        process = subprocess.Popen(['which', tool], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out = process.communicate()[0]
        if out == "":
            return False
    except (OSError, subprocess.CalledProcessError) as e:
        return False
    return True


def print_line(string, new_line=False):
    if new_line:
        string += '\n'
    else:
        string += '\r'
    sys.stdout.write(string)
    sys.stdout.flush()


def print_timer(complete, current):
    share = current * 100.0 / complete

    string = '  {:6.2f}%'.format(share)
    if complete == current:
        string = colorize(string, 'green')

    string += ' ['
    string += '=' * int(share / 10 * 3)
    string += ' ' * (30 - int(share / 10 * 3))
    string += '] {:6.1f}s remaining'.format(complete - current)

    print_line(string, new_line=complete == current)


def sleep_progress_bar(seconds, current_time, complete):
    print_timer(complete=complete, current=current_time)
    while seconds > 0:
        time.sleep(min(1, seconds))
        current_time = current_time + min(1, seconds)
        print_timer(complete=complete, current=current_time)
        seconds -= 1
    return current_time


def parse_bw(bw_str):
    """
    Parse a bandwidth that is given as str (e.g. "15mbit") and return it as int value, i.e. as a number in mbps (e.g. 15).

    :param bw_str: The bandwidth as str, as given in the config file
    :type bw_str: str

    :return: The bandwidth as int
    :rtype: int
    """
    if bw_str.endswith('mbit'):
        separator = 'm'
    else:
        print_warning('Illegal bandwidth string: {}. Using default ({})'.format(bw_str, DEFAULT_BANDWIDTH))
        return DEFAULT_BANDWIDTH
    split = bw_str.split(separator)
    return int(split[0])


def parse_rtt(rtt_str):
    """
    Parse an RTT that is given as str (e.g. "42ms") and return it as int value, i.e. as a number in ms (e.g. 42).

    :param rtt_str: The RTT as str, as given in the config file
    :type rtt_str: str

    :return: The RTT as int
    :rtype: int
    """
    split = rtt_str.split('m')
    rtt = int(split[0])  # Before the 'm' is only the number
    return rtt


def parse_bufsize(bufsize_str, bw=None, rtts=None, base_delay=0):
    """
    Parse a buffer size that is given as str (e.g. "20packets" or "1bdp") and return it as int value, i.e. as a number 
        in packets (e.g. 20).

    If the bufsize str specifies the bufsize in BDPs, you need the link bandwidth and a list of RTTs in order to be able
        to compute the BDP. If the bufsize str specifies the bufsize in packets, none of these are necessary and the 
        parameters will be ignored.

    :param bufsize_str: The buffer size as str, as given in the config file
    :type bufsize_str: str
    :param bw: The link bandwidth. Only required if the bufsize_str specifies the buffer size in BDPs
    :type bw: int
    :param rtts: A list containing the RTT of every host, as returned by parse_host_commands. Only required if the
        bufsize_str specifies the buffer size in BDPs
    :type rtts: list(str)
    :param base_delay: Base delay that the network might have in ms. For example, if a mininet is configured such that 
        every link has a small delay and they sum up to 4ms delay, you should specify this parameter as 4.
    :type base_delay: int

    :return: The buffer size as int
    :rtype: int
    """
    if bufsize_str.endswith('packets'):
        split = bufsize_str.split('p')
        bufsize = int(split[0])

    elif bufsize_str.endswith('bdp'):
        split = bufsize_str.split('b')
        bufsize_in_bdps = float(split[0])

        # Convert BDP to # packets
        # Use the maximum RTT value for computing the "BDP" of this link
        max_rtt_in_list = 0
        for rtt in rtts:
            value = parse_rtt(rtt)
            if value > max_rtt_in_list:
                max_rtt_in_list = value

        max_rtt_ms = max_rtt_in_list + base_delay  # RTT in ms
        max_rtt_s = max_rtt_ms / 1000  # RTT in s

        bdp_mbit = bw * max_rtt_s  # 1 BDP in Mbit (from bw in Mbit/s and RTT in s)
        bdp_kbit = bdp_mbit * 1000  # 1 BDP in Kbit
        bdp_kb = bdp_kbit / 8  # 1 BDP in KByte
        bdp_packets = bdp_kb / 1.5  # 1 BDP in packets, assuming that one packet = 1500 Byte (Ethernet frame size)

        bufsize = int(bufsize_in_bdps * bdp_packets)  # Buffer size in packets
    else:
        print_warning('Illegal buffer size string: {}. Using default ({})'.format(bufsize_str, DEFAULT_BUFFER_SIZE))
        bufsize = DEFAULT_BUFFER_SIZE
    return bufsize


def parse_link_commands(commands, rtts=None, base_delay=0):
    """
    Parse a list of commands and retrieve information about links.

    :param commands: The list of commands, as obtained from parseConfigFile()
    :type commands: list(dict)
    :param rtts: A list containing the RTT of every host, as returned by parse_host_commands. This list is needed if
        the buffer size is specified in BDPs
    :type rtts: list(str)
    :param base_delay: Base delay that the network might have in ms. For example, if a mininet is configured such that 
        every link has a small delay and they sum up to 4ms delay, you should specify this parameter as 4.
    :type base_delay: int

    :return: the bottleneck bandwidth in mbit/s and buffer size in packets
    :rtype: int, int
    """
    bandwidth = DEFAULT_BANDWIDTH
    bufsize = DEFAULT_BUFFER_SIZE

    for cmd in commands:
        if cmd['command'] != 'link':
            continue

        if cmd['change'] == 'bw':
            bandwidth = parse_bw(cmd['value'])
        elif cmd['change'] == 'bufsize':
            bufsize = parse_bufsize(cmd['value'], bandwidth, rtts, base_delay)

    return bandwidth, bufsize


def parse_host_commands(commands):
    """
    Parse a list of commands and retrieve information about hosts.

    :param commands: The list of commands, as obtained from parseConfigFile()
    :type commands: list(dict)

    :return: the experiment duration in seconds, number of hosts, a list containing the RTT of every host, a list 
        containing the congestion control algorithm of every host, a list containing the start times of each hosts'
        data stream, and a list containing the stop times of each hosts' data stream
    :rtype: int, int, list(str), list(str), list(float), list(float)
    """
    duration = 0
    n = 0
    rtts = []
    congestion_controls = []
    starts = []
    stops = []

    for cmd in commands:
        if cmd['command'] == 'host':
            n += 1
            rtts.append(cmd['rtt'])
            congestion_controls.append(cmd['algorithm'])
            starts.append(cmd['start'])
            stops.append(cmd['stop'])

            if cmd['start'] + cmd['stop'] > duration:
                duration = cmd['start'] + cmd['stop']

    return int(duration), n, rtts, congestion_controls, starts, stops


def parseConfigFile(file):
    """
    Parse a config file that contains commands defining the experiment setup.

    :param file: Path to the config file
    :type file: os.PathLike

    :return: a list of commands, where each command corresponds to a line in the config file and is stored as a dict.
        The dict contains the key "command", which is either host or link.
        If host, the dict contains keys "algorithm" (the congestion control algorithm for this host), "rtt" (the RTT
            of this host), "start" and "stop" (when to start and stop sending with this host).
        If link, the dict contains keys "change" (the variable to change, e.g. bw or bufsize), "value" (the value to
            set the variable to), and "start" (when to start using that particular value)
    :rtype: list(dict)
    """
    #cc_algorithms = get_available_algorithms()

    unknown_alorithms = []
    number_of_hosts = 0
    output = []
    f = open(file)
    for line in f:
        line = line.replace('\n', '').strip()

        if len(line) > 1:
            if line[0] == '#':
                continue

        split = line.split(',')
        if split[0] == '':
            continue
        command = split[0].strip()

        if command == 'host':
            if len(split) != 5:
                print_warning('Too few arguments to add host in line\n{}'.format(line))
                continue
            algorithm = split[1].strip()
            rtt = split[2].strip()
            start = float(split[3].strip())
            stop = float(split[4].strip())
            # if algorithm not in cc_algorithms:
            #     if algorithm not in unknown_alorithms:
            #         unknown_alorithms.append(algorithm)
            #     continue

            if number_of_hosts >= MAX_HOST_NUMBER:
                print_warning('Max host number reached. Skipping further hosts.')
                continue

            number_of_hosts += 1
            output.append({
                'command': command,
                'algorithm': algorithm,
                'rtt': rtt,
                'start': start,
                'stop': stop})

        elif command == 'link':
            if len(split) != 4:
                print_warning('Too few arguments to change link in line\n{}'.format(line))
                continue
            change = split[1].strip()
            if change not in ('bw', 'rtt', 'loss', 'bufsize'):
                print_warning('Unknown link option "{} in line\n{}'.format(change, line))
                continue
            value = split[2].strip()
            start = float(split[3].strip())
            output.append({
                'command': command,
                'change': change,
                'value': value,
                'start': start
            })
        else:
            print_warning('Skip unknown command "{}" in line\n{}'.format(command, line))
            continue

    # if len(unknown_alorithms) > 0:
    #     print_warning('Skipping uninstalled congestion control algorithm:\n  ' + ' '.join(unknown_alorithms))
    #     print_warning('Available algorithms:\n  ' + cc_algorithms.strip())
    #     print_warning('Start Test anyway in 10s. (Press ^C to interrupt)')
    #     try:
    #         time.sleep(10)
    #     except KeyboardInterrupt:
    #         sys.exit(1)

    return output


def store_experiment_configuration(output_directory, name, bandwidth, bufsize, commands):
    """
    Store the experiment configuration into a file for better reproducibility.

    :param output_directory: Path to the directory in which the test results, and hence also the config, should be stored
    :type output_directory: os.PathLike
    :param name: The experiment name
    :type name: str
    :param bandwidth: The bottleneck bandwidth
    :type bandwidth: str
    :param bufsize: The buffer size
    :type bufsize: str
    :param commands: The list of commands (i.e. the parsed config file) as obtained by parseConfigFile()
    :type commands: list(dict)
    """
    config = [
        'Test Name: {}'.format(name),
        'Date: {}'.format(time.strftime('%c')),
        'Kernel: {}'.format(get_host_version()),
        # 'Git Commit: {}'.format(get_git_revision_hash()),
        'Initial Bandwidth: {}'.format(bandwidth),
        'Initial Burst Buffer: {}'.format(bufsize),
        'Commands: '
    ]

    for cmd in commands:
        config_line = '{}, '.format(cmd['command'])

        if cmd['command'] == 'link':
            config_line += '{}, {}, {}'.format(cmd['change'], cmd['value'], cmd['start'])
        elif cmd['command'] == 'host':
            config_line += '{}, {}, {}, {}'.format(cmd['algorithm'], cmd['rtt'], cmd['start'], cmd['stop'])

        config.append(config_line)

    with open(os.path.join(output_directory, 'parameters.txt'), 'w') as f:
        f.write('\n'.join(config))


