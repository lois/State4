"""
Utility classes and functions for plotting results of TCPlog.
"""

from ipaddress import ip_address

from collections import defaultdict, OrderedDict

import os


# Copied from the original TCPplot
class TcpLogSample:
    """
    Container for a single TCPlog sample, containing all important recorded values.
    """
    def __init__(self, line, time_offset, layout = 1):
        ## #FORMAT: time srcIp srcPort dstIp dstPort cwnd rwnd sst rtt bw [loss]

        input_list = line.strip().split(" ")

        if layout == 1:
            self.reltime = float( input_list[0] )
            self.abstime = self.reltime + time_offset

            self.src_ip = ip_address( input_list[1] )
            self.src_port = int( input_list[2] )

            self.dst_ip = ip_address( input_list[3] )
            self.dst_port = int( input_list[4] )

            self.cwnd = int( input_list[5] )
            self.rwnd = int( input_list[6] )
            self.ssthresh = int( input_list[7] )
            self.rtt = float( input_list[8] ) / 1000.0
            self.goodput = float( input_list[9] )

            if ( len(input_list) > 10 ):
                self.loss = float( input_list[-1] )
            else:
                self.loss = None
        elif layout == 2:  # this is the long and latest layout that contains everything
            self.reltime = float(input_list[1])
            self.abstime = float(input_list[0])

            self.src_ip = ip_address(input_list[2])
            self.src_port = int(input_list[3])

            self.dst_ip = ip_address(input_list[4])
            self.dst_port = int(input_list[5])

            self.cwnd = int(input_list[6])
            #self.rwnd = int(input_list[6])
            self.ssthresh = int(input_list[7])
            self.rtt = float(input_list[8])
            if input_list[13] != "_":
                self.goodput = float(input_list[13])
            else:
                self.goodput = 0
            if input_list[-1] != "_":
                self.loss = float(input_list[-1])
            else:
                self.loss = 0

    def is_ssh(self):
        return self.src_port == 22 or self.dst_port == 22

    def to_list(self):
        return [ self.reltime, self.abstime, self.src_ip, self.src_port, self.dst_ip, self.dst_port,
                 self.cwnd, self.rwnd, self.ssthresh, self.rtt, self.loss ]

    def get_grouping_value(self):
        return "%s:%s" % (self.src_port, self.dst_port)

    def __str__(self):
        return " ".join( (str(x) for x in self.to_list()) )


# Copied from original TCPplot
def read_tcp_log_header( file_object ):
    def __read():
        return file_object.readline().strip()

    header = OrderedDict()

    ## read header
    line = __read()
    assert line == "#TCPLOG"

    while not line.startswith("#END_HEADER"):
        split_line = line.split(" ", 1)
        if ( len(split_line) == 1 ):
            split_line.append("[Present]")

        header[ split_line[0] ] = split_line[1]

        line = __read()

    ## Print header
    for key in header:
        print( "{} {}".format(key, header[key]) )

    ## get required data
    timestamp = float( header["#TIMESTAMP:"] )
    format = header["#FORMAT:"]

    return timestamp, format


# Copied from original TCPplot
def get_opener(filename):
    if filename.endswith(".bz2"):
        import bz2
        return bz2.open
    else:
        return open


# Copied from original TCPplot
def get_time_offset(sample):
    if ( str(sample.src_ip) in ("10.0.1.2", "10.0.2.2") ):
        return 60.0
    else:
        return 0.0


# Copied from original TCPplot
def read_file(filename, sampling=None):
    connections = defaultdict(list)
    last_sample = defaultdict( lambda: float("-inf") )

    ## automatically handle compressed files
    open_func = get_opener(filename)
    with open_func(filename, mode="tr", encoding="UTF-8") as f:

        timestamp, format = read_tcp_log_header( f )
        assert( format == "time srcIp srcPort dstIp dstPort cwnd rwnd sst rtt bw" or
                format == "time srcIp srcPort dstIp dstPort cwnd rwnd sst rtt bw loss" or
                format == "absoluteTimestamp relativeTimestamp srcIp srcPort dstIp dstPort cwnd sst rtt minRtt maxRtt avgRtt meanRtt throughput smoothedThroughput assumedLosses")

        layout = 1
        if format == "absoluteTimestamp relativeTimestamp srcIp srcPort dstIp dstPort cwnd sst rtt minRtt maxRtt avgRtt meanRtt throughput smoothedThroughput assumedLosses":
            layout = 2

        for line in f:
            ## skip comments and empty lines
            if ( line.strip() == "" ):
                continue
            if ( line.startswith == "#" ):
                continue

            ## Parse Line
            sample = TcpLogSample(line, timestamp, layout)

            ## generally exclude SSH
            if sample.is_ssh():
                continue

            ## To which connection does this line belong to?
            conn_key = sample.get_grouping_value()

            ## Sampling: drop line if timestamps are too close together
            if ( sampling ):
                if ( sample.reltime < last_sample[conn_key] + sampling ):
                    continue
                last_sample[conn_key] = sample.reltime

            ## adjust timestamp (if needed)
            sample.reltime += get_time_offset(sample)

            ## store values per connection
            connections[conn_key].append(sample)

    return connections


def get_tl_files(directory):
    """
    Find all tcplog log files in a directory. This assumes that tcplog files are named like "tcplog-h0.log".

    :param directory: The directory to search in
    :type directory: os.PathLike

    :return: a list of names of tcplog files, e.g. ["tcplog-h0.log", "tcplog-h1.log"]
    :rtype: list(str)
    """
    dir_contents = os.listdir(directory)
    tl_files = [f for f in dir_contents if f.startswith('tcplog') and f.endswith('.log') and os.path.isfile(os.path.join(directory, f))]
    tl_files = sorted(tl_files)
    return tl_files


def get_tcp_log_sample_list(directory, tl_files):
    """
    Obtain lists of TcpLogSamples from the tcplog files in a directory.

    :param directory: The directory in which the tcplog files (tl_files) are located
    :type directory: os.PathLike
    :param tl_files: List of names of tcplog files that are located inside the directory
    :type tl_files: list(str)

    :return: a list of list of TcpLogSamples. I.e. for each host in the experiment (and thus for each tcplog file) there is one list of TcpLogSamples
    :rtype: list(list(TcpProbeSample.TcpLogSample))
    """
    samples = []

    for f in tl_files:
        connections = read_file(os.path.join(directory, f))

        # TODO Currently we only support len(connections) == 1; if multiple connections are sent from one host, this must be changed

        # keys() contains just one element; the only connection that exists. Thus, the following hack works
        the_key = None
        for k in connections.keys():
            print(k)
            the_key = k
        connection = connections[the_key]  # connection is a list of TcpProbeSample.TcpLogSample objects for the current host

        samples.append(connection)  # samples should then contain such a list for each host

    return samples

