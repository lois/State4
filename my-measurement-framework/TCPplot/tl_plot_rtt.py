#!/usr/bin/env python3

from util import get_tl_files, get_tcp_log_sample_list

import argparse
import os

import matplotlib.pyplot as plt


DEFAULT_OUTPUT_FILE_NAME = 'plot_rtt.pdf'


def parse_args():
    """
    Parse command line arguments.
    """
    parser = argparse.ArgumentParser(description='Parse tcplog files and create a plot that shows the RTTs of the flows.')

    parser.add_argument('directory', help='path to the directory that contains test results', nargs=1)
    parser.add_argument('-s', '--savefig', help='save the plot into a file. If a name is provided to this argument (i.e. "-s name.pdf"), it will be used as file name for the plot, ' + \
            'else (i.e. "-s") the name will be {}. Omit this option to display the plot directly'.format(DEFAULT_OUTPUT_FILE_NAME), nargs='?', const=DEFAULT_OUTPUT_FILE_NAME, default=None)

    parser.add_argument('-l', '--losses', help='plot packet losses, too', action='store_true')

    args = parser.parse_args()
    return args


def get_rtt_data(tcp_log_sample_list):
    """
    Parse a list of lists of TcpLogSamples and obtain the timestamps and rtt values.

    :param tcp_log_sample_list: A list of lists of TcpLogSamples, as returned by get_tcp_log_sample_list(). This list contains for each host a list of samples
    :type tcp_log_sample_list: list(list(TcpProbeSample.TcpLogSample))

    :return: a list of lists of tuples that contain the timestamp and rtt values. Again, this list contains for each host a list of tuples
    :rtype: list(list(tuple(float, float)))
    """
    rtt_data = []

    for samples in tcp_log_sample_list:
        cur_samples = []

        for s in samples:
            cur_samples.append((s.reltime, s.rtt))

        rtt_data.append(cur_samples)

    return rtt_data


def get_losses_data(tcp_log_sample_list):
    """
    Parse a list of lists of TcpLogSamples and obtain the timestamps of packet losses.

    :param tcp_log_sample_list: A list of lists of TcpLogSamples, as returned by get_tcp_log_sample_list(). This list contains for each host a list of samples
    :type tcp_log_sample_list: list(list(TcpProbeSample.TcpLogSample))

    :return: a list of timestamps at which packets losses occur
    :rtype: list(float)
    """
    timestamps = []

    for samples in tcp_log_sample_list:
        prev_losses = 0
        for s in samples:
            cur_losses = s.loss
            if cur_losses > prev_losses:
                timestamps.append(s.reltime)
                prev_losses = cur_losses

    return sorted(timestamps)


def plot_rtt(rtt_data, savefig, loss_timestamps=None):
    """
    Use matplotlib to create a rtt plot.

    :param rtt_data: a list of lists of (timestamp, rtt) tuples
    :type rtt_data: list(list(tuple(float, float)))

    :param savefig: The path to the file to store the plot into. If savefig is None, the plot will be displayed directly ("live")
    :type savefig: os.PathLike or None

    :param loss_timestamps: List of limestamps at which packet losses occured. If loss_timestamps is None, losses will not be plotted
    :type loss_timestamps: list(float) or None
    """
    host_counter = 0
    max_rtt_value = 0  # We may need this for plotting losses at the very top of the plot
    for r in rtt_data:
        # y-values that are 0 usually only occur when a flow has not started yet, hence don't plot them
        x_values = [tup[0] for tup in r if tup[1] > 0]
        y_values = [tup[1] for tup in r if tup[1] > 0]

        label = 'h{}'.format(host_counter)

        plt.plot(x_values, y_values, label=label)

        host_counter += 1

        max_rtt_of_cur_host = max(y_values)
        if max_rtt_of_cur_host > max_rtt_value:
            max_rtt_value = max_rtt_of_cur_host

    if loss_timestamps:
        x_values = loss_timestamps
        y_values = [max_rtt_value + 1] * len(x_values)

        plt.plot(x_values, y_values, marker='|', color='red', linestyle='None')

    plt.xlabel('Time [s]')
    plt.ylabel('RTT [ms]')

    plt.grid(axis='x', b=False)
    plt.grid(axis='y', b=True)

    plt.tight_layout()

    plt.legend()
    plt.xlim([5,20])

    if savefig:
        plt.savefig(savefig)
    else:
        plt.show()


def main():
    args = parse_args()

    directory = args.directory[0]
    if args.savefig is not None:
        savefig = os.path.join(directory, args.savefig)
    else:
        savefig = None

    tl_files = get_tl_files(directory)
    tcp_log_sample_list = get_tcp_log_sample_list(directory, tl_files)

    rtt_data = get_rtt_data(tcp_log_sample_list)

    loss_timestamps = get_losses_data(tcp_log_sample_list) if args.losses else None

    plot_rtt(rtt_data, savefig, loss_timestamps)


if __name__ == '__main__':
    main()

