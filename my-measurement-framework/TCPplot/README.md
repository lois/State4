# TCPplot

This folder contains files for creating plots from TCPlog files.

- `tl_plot_cwnd.py`: Creates a cwnd plot that contains the congestion window of all hosts
- `tl_plot_rtt.py`: Creates an rtt plot that contains the round-trip time of all hosts

All scripts reads the contents of an experiment result directory that was created by `run_mininet.py` or `run_testbed.py` and parse the TCPlog files in it.
