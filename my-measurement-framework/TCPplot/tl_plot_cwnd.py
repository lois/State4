#!/usr/bin/env python3

from util import read_file, get_tl_files, get_tcp_log_sample_list

import argparse
import os

import matplotlib.pyplot as plt


DEFAULT_OUTPUT_FILE_NAME = 'plot_cwnd.pdf'


def parse_args():
    """
    Parse command line arguments.
    """
    parser = argparse.ArgumentParser(description='Parse tcplog files and create a plot that show the CWNDs of the flows.')

    parser.add_argument('directory', help='path to the directory that contains test results', nargs=1)
    parser.add_argument('-s', '--savefig', help='save the plot into a file. If a name is provided to this argument (i.e. "-s name.pdf"), it will be used as file name for the plot, ' + \
            'else (i.e. "-s") the name will be {}. Omit this option to display the plot directly'.format(DEFAULT_OUTPUT_FILE_NAME), nargs='?', const=DEFAULT_OUTPUT_FILE_NAME, default=None)

    args = parser.parse_args()
    return args


def get_cwnd_data(tcp_log_sample_list):
    """
    Parse a list of lists of TcpLogSamples and obtain the timestamps and cwnd values.

    :param tcp_log_sample_list: A list of lists of TcpLogSamples, as returned by get_tcp_log_sample_list(). This list contains for each host a list of samples
    :type tcp_log_sample_list: list(list(TcpProbeSample.TcpLogSample))

    :return: a list of lists of tuples that contain the timestamp and cwnd values. Again, this list contains for each host a list of tuples
    :rtype: list(list(tuple(float, int)))
    """
    cwnd_data = []

    for samples in tcp_log_sample_list:
        cur_samples = []

        for s in samples:
            cur_samples.append((s.reltime, s.cwnd))

        cwnd_data.append(cur_samples)

    return cwnd_data


def plot_cwnd(cwnd_data, savefig=None):
    """
    Use matplotlib to create a cwnd plot.

    :param cwnd_data: a list of lists of (timestamp, cwnd) tuples
    :type cwnd_data: list(list(tuple(float, int)))

    :param savefig: The path to the file to store the plot into. If savefig is None, the plot will be displayed directly ("live")
    :type savefig: os.PathLike or None
    """
    host_counter = 0
    for c in cwnd_data:
        # y-values that are 0 usually only occur when a flow has not started yet, hence don't plot them
        x_values = [tup[0] for tup in c if tup[1] > 0]
        y_values = [tup[1] for tup in c if tup[1] > 0]

        label = 'h{}'.format(host_counter)

        plt.plot(x_values, y_values, label=label)

        host_counter += 1

    plt.xlabel('Time [s]')
    plt.ylabel('CWND [MSS]')

    plt.grid(axis='x', b=False)
    plt.grid(axis='y', b=True)

    plt.tight_layout()

    plt.legend()

    if savefig:
        plt.savefig(savefig)
    else:
        plt.show()


def main():
    args = parse_args()

    directory = args.directory[0]
    if args.savefig is not None:
        savefig = os.path.join(directory, args.savefig)
    else:
        savefig = None

    tl_files = get_tl_files(directory)
    tcp_log_sample_list = get_tcp_log_sample_list(directory, tl_files)
    cwnd_data = get_cwnd_data(tcp_log_sample_list)

    plot_cwnd(cwnd_data, savefig)


if __name__ == '__main__':
    main()

