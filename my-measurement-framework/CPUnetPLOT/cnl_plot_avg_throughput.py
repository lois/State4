#!/usr/bin/env python3

from cnl_library import get_cnl_files, get_throughput_data
from cnl_plot_jains_fairness_index import calculate_jains_index

import argparse
import os

import numpy as np
import matplotlib.pyplot as plt


DEFAULT_OUTPUT_FILE_NAME = 'plot_average_throughput_and_jains_index.pdf'

BUFFER_SIZES = [0.1, 0.2, 0.3, 0.5, 1, 2, 4, 8, 16]  # EDIT THIS LIST WHEN NECESSARY
MARKERS = ['v', '^', '<', '>', 'o', 's', '*', 'H']


def parse_args():
    """
    Parse command line arguments.
    """
    parser = argparse.ArgumentParser(description='Create a plot that shows the average throughput of experiments with varying buffer sizes, as well as Jain\'s fairness index for each experiment')

    parser.add_argument('directories', nargs=len(BUFFER_SIZES), help='list of directories to check, in order of ascending buffer sizes. Each directory should contain at least one subdirectory, where each subdirectory contains the results of one experiment run. You should provide as many directories as there are buffer sizes. To change this number, edit the BUFFER_SIZES constant in this script')

    parser.add_argument('-s', '--savefig', help='save the plot into a file. If a name is provided to this argument (i.e. "-s name.pdf"), it will be used as file name for the plot, ' + \
            'else (i.e. "-s") the name will be {}. Omit this option to display the plot directly'.format(DEFAULT_OUTPUT_FILE_NAME), nargs='?', const=DEFAULT_OUTPUT_FILE_NAME, default=None)

    parser.add_argument('-l', '--labels', nargs='+', help='the label texts for the flows')

    args = parser.parse_args()
    return args


def compute_average_throughputs(all_throughput_data):
    """
    For each experiment, compute the average throughput of every host.

    :param all_throughput_data: list containing a list pieces of throughput data; one list for each buffer size, containing one pieces of throughput data
        for each experiment, where throughput data is a list of hosts, for each host a list of (timestamp, throughput) tuples
    :type all_throughput_data: list(list(list(list(tuple(float, float)))))

    :return: a list containing for every experiment the average troughput of each host; one list for each buffer size, containing a list of experiments, 
        for each experiment a list of hosts, for each host the average throughput
    :rtype: list(list(list(float)))
    """
    average_throughputs = []

    for cur_buffer_size_throughput_data in all_throughput_data:  # Iterate buffer sizes
        cur_buffer_size_throughputs = []
        for throughput_data in cur_buffer_size_throughput_data:  # get "throughput data" pieces, i.e. iterate experiments
            average_throughput_per_host = []
            for host in throughput_data:  # get each host of the current throughput data piece
                throughput_sum = 0
                number_of_logs = 0
                for tup in host:  # get (timestamp, throughput) tuples for the current host
                    # Exclude samples from Startup phase, i.e. from the first 5 seconds
                    if tup[0] < 5:
                        continue

                    cur_throughput_bps = tup[1]
                    cur_throughput_mbps = cur_throughput_bps / (10**6)  # Convert from bit/s to mbit/s
                    throughput_sum += cur_throughput_mbps
                    number_of_logs += 1

                average_throughput = throughput_sum / number_of_logs
                average_throughput_per_host.append(average_throughput)

            cur_buffer_size_throughputs.append(average_throughput_per_host)
        average_throughputs.append(cur_buffer_size_throughputs)

    return average_throughputs


def plot(average_throughputs, number_of_hosts, savefig=None, labels=None):
    """
    Use matplotlib to create an average throughput plot.

    :param average_throughputs: list of lists of average throughput lists, one list for each buffer size containing one list for each experiment, for each
        experiment a list of hosts, for each host the average throughput
    :type average_throughputs: list(list(list(float)))
    :param number_of_hosts: the number of hosts in each experiment
    :type number_of_hosts: int

    :param savefig: The path to the file to store the plot into. If savefig is None, the plot will be displayed directly ("live")
    :type savefig: os.PathLike or None

    :param labels: A list of labels to use for describing the flows
    :type labels: list(str)
    """
    x_values = np.array(BUFFER_SIZES).astype('str')  # astype('str') is a hack to evenly space the x ticks

    fig, ax_throughput = plt.subplots()

    for host in range(number_of_hosts):
        y_values = []
        for bufsize in average_throughputs:
            data = []
            for experiment in bufsize:
                data.append(experiment[host])

            info_dict = {'min': min(data), 'max': max(data), 'avg': sum(data)/len(data)}
            y_values.append(info_dict)

        mins = [y['min'] for y in y_values]
        maxs = [y['max'] for y in y_values]
        avgs = [y['avg'] for y in y_values]
        print(avgs)

        if labels is None:
            label = 'Flow {}'.format(host)
        else:
            label = labels[host]

        ax_throughput.plot(x_values, avgs, label=label, marker=MARKERS[host])
        ax_throughput.fill_between(x_values, mins, maxs, alpha=0.4) 
    
    ax_throughput.set_xlabel('Buffer size [BDP]')
    ax_throughput.set_ylabel('Throughput [Mbit/s]')
    ax_throughput.set_xticks(x_values)
    ax_throughput.set_ylim([0, 100])

    ax_throughput.grid(axis='x', b=True)
    ax_throughput.grid(axis='y', b=False)

    fig.tight_layout()

    lines, labels = ax_throughput.get_legend_handles_labels()
    ax_throughput.legend(lines, labels, loc='lower left')

    if savefig:
        plt.savefig(savefig)
    else:
        plt.show()


def main():
    args = parse_args()

    directories = args.directories
    if args.savefig is not None:
        savefig = args.savefig
    else:
        savefig = None

    all_throughput_data = []
    for directory in directories:  # Iterate through directories for different buffer sizes
        cur_buffer_size_throughput_data = []
        subdirectories = [f.path for f in os.scandir(directory) if f.is_dir()]
        for subdir in subdirectories:  # For one buffer size, iterate through the experiments
            cnl_files = get_cnl_files(subdir)
            throughput_data = get_throughput_data(cnl_files)
            cur_buffer_size_throughput_data.append(throughput_data)
        all_throughput_data.append(cur_buffer_size_throughput_data)

    average_throughputs = compute_average_throughputs(all_throughput_data)
    number_of_hosts = len(average_throughputs[0][0])

    plot(average_throughputs, number_of_hosts, savefig, args.labels)


if __name__ == '__main__':
    main()

