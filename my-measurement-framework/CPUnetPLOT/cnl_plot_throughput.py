#!/usr/bin/env python3

from cnl_library import get_cnl_files, get_throughput_data

import argparse
import os

import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px
import plotly.graph_objects as go


DEFAULT_OUTPUT_FILE_NAME = 'plot_throughput.pdf'
# DEFAULT_PLOT_TOOL = 'plotly'

DEFAULT_PLOT_TOOL = 'matplotlib'
# COLORS = ['#d73027','#fc8d59','#fee090','#e0f3f8','#91bfdb','#4575b4']
COLORS = None
def parse_args():
    """
    Parse command line arguments.
    """
    parser = argparse.ArgumentParser(description='Parses cpunetlog log files and plots the throughput.')
    # host names should be 
    parser.add_argument('--host-names', help='host names for legends of the plot, format example: h1,h2,h3,h4',
                            required=False, type=str,
                            default="h1")
    
    parser.add_argument('directory', help='path to the directory that contains test results', nargs=1)
    parser.add_argument('-s', '--savefig', help='save the plot into a file. If a name is provided to this argument (i.e. "-s name.pdf"), it will be used as file name for the plot, ' + \
            'else (i.e. "-s") the name will be {}. Omit this option to display the plot directly'.format(DEFAULT_OUTPUT_FILE_NAME), nargs='?', const=DEFAULT_OUTPUT_FILE_NAME, default=None)

    args = parser.parse_args()
    return args

def plot_throughput(throughput_data, savefig=None):
    """
    Use matplotlib to create a throughput plot.

    :param throughput_data: list of throughput data (one piece of data for each host)
    :type throughput_data: list(list(tuple(float, float)))

    :param savefig: The path to the file to store the plot into. If savefig is None, the plot will be displayed directly ("live")
    :type savefig: os.PathLike or None
    """
    host_counter = 1
    total = []
    data = []
    font = {'size'   : 16}

    plt.rc('font', **font)
    plt.rc('pdf',fonttype=42)
    # plt.rc('font', family='serif') 
    for t in throughput_data:
        # y-values that are 0 usually only occur when a flow has not started yet, hence don't plot them (via tup[1] > 0 condition)
        
        x_values = [tup[0] for tup in t ]
        y_values = [tup[1]/(10**6) for tup in t ]  # Includes a conversion from bit/s to mbit/s

        if(total == []):
            x_vals = x_values
            total = y_values
        else:
            print(len(total), len(y_values))
            length= min(len(total),len(y_values))
            for i in range(0,length):
                total[i] = total[i] + y_values[i]
        # if host_counter == 2: 
        label = 'Sender {}'.format(host_counter)
        if host_counter == 1:
            label='Swing State'
            color = '#1f77b4'
        elif host_counter == 2:
            label='State4'
            color = 'orange'
            # color = '#e377c2'
        if(DEFAULT_PLOT_TOOL == 'plotly'):
            plot = go.Scatter(x = x_values,
                              y = y_values,
                              mode = "lines",
                              name = label)
            data.append(plot)
        else:
            if(COLORS):
                plt.plot(x_values, y_values, COLORS[host_counter-1],label=label )
            else:
                plt.plot(x_values, y_values, color,label = label)
        host_counter += 1

        # host_counter += 1

    label = 'Sum'   
    # print("total throughput", total)
    if(DEFAULT_PLOT_TOOL == 'plotly'):
        if(len(throughput_data) > 1): # plot sum with more than 1 sender
            plot = go.Scatter(x = x_vals, y = total, mode = "lines", name = label)
        data.append(plot)
        fig = go.Figure(data = data)
        fig.update_xaxes(range=[0,30])
        fig.write_image(savefig)

    else:
        # if(len(throughput_data) > 1): # plot sum with more than 1 sender
        #     if(COLORS):
        #         plt.plot(x_vals, total,colors =COLORS[-1], label= label)
        #     else:
        #         plt.plot(x_vals, total, label= label)

        plt.xlabel('Time [s]')
        plt.ylabel('Throughput [Mbit/s]')


        plt.grid(axis='x', b=False)
        plt.grid(axis='y', b=True)

        plt.tight_layout()

        plt.legend(loc='upper right')    
        plt.xlim([0,30])
        plt.ylim([0,625])
        # plt.xlim([15,16])
        # plt.xlim([15.4,15.6])
        # plt.xlim([15.45,15.55])
        if savefig:
            plt.savefig(savefig)
        else:
            plt.show()



def main():
    args = parse_args()

    directory = args.directory[0]
    if args.savefig is not None:
        savefig = os.path.join(directory, args.savefig)
    else:
        savefig = None

    host_names = args.host_names.split(',')
    


    cnl_files = get_cnl_files(directory)
    throughput_data = get_throughput_data(cnl_files)

    plot_throughput(throughput_data, savefig)


if __name__ == '__main__':
    main()

