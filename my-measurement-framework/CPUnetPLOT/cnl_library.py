#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Copyright (c) 2014,
# Karlsruhe Institute of Technology, Institute of Telematics
#
# This code is provided under the BSD 2-Clause License.
# Please refer to the LICENSE.txt file for further information.
#
# Author: Mario Hock


from io import StringIO

import json
import csv
import os

## Helper functions for CNLParser -- but they could also be handy in other contexts.

def cnl_slice(file, start_delimiter, end_delimiter):

    ## Find beginning
    for line in file:
        if ( line.startswith(start_delimiter) ):
            break

    ## Skip comments and quit on end
    for line in file:
        if ( line.startswith(end_delimiter) ):
            return

        # skip empty or commented lines
        if ( not line or line[0] == "%" or line[0] == "#" ):
            continue

        yield line


def create_csv_index(csv_header):
    ## Create an index that maps csv_header names to tuple indices.
    csv_field_index = dict()
    i = 0
    for field in csv_header:
        csv_field_index[field] = i
        i += 1

    return csv_field_index


def read_header(f):
    str_io = StringIO()

    for line in cnl_slice(f, "%% Begin_Header", "%% End_Header"):
        str_io.write(line)

    str_io.seek(0)
    header = json.load( str_io )

    return header


class CNLParser:
    class WrongFileFormat_Exception(Exception):
        pass


    def __init__(self, filename):
        self.filename = filename
        #print (filename)

        if ( os.path.isdir(self.filename) ):
            raise self.WrongFileFormat_Exception()

        ## automatically handle compressed files
        if self.filename.endswith(".bz2"):
            import bz2
            self.open_func = bz2.open
        else:
            self.open_func = open


        with self.open_func( self.filename, mode="tr", encoding="UTF-8" ) as in_file:
            try:
                ## Check file format version.
                if ( not in_file.readline() == "%% CPUnetLOGv1\n" ):
                    raise self.WrongFileFormat_Exception()

                ## Read JSON header.
                self.header = read_header(in_file)

                ## Read CSV "header"
                csv_reader = csv.reader( cnl_slice(in_file, "%% Begin_Body", "%% End_Body"), skipinitialspace=True )
                self.csv_header = next(csv_reader)
                self.csv_index = create_csv_index(self.csv_header)
            except UnicodeDecodeError:
                raise self.WrongFileFormat_Exception()


    def get_csv_iterator(self, fields=None):
        """
        Returns an iterator to get the csv-values line by line.

        @param fields [list] Only the "columns" specified in |fields| are included in the returned list (in that order).
                      [None] All "columns" are included (order defined by |self.csv_header|.
        """

        indices = None

        ## Only return selected columns (if the |fields| option is set).
        if ( fields ):
            indices = self.get_csv_indices_of(fields)


        ## Read from file.
        with self.open_func( self.filename, mode="tr", encoding="UTF-8" ) as in_file:
            ## Find start of the CSV part.
            csv_reader = csv.reader( cnl_slice(in_file, "%% Begin_Body", "%% End_Body"), skipinitialspace=True )
            csv_header = next(csv_reader)
            assert( csv_header == self.csv_header )

            ## TODO convert every field to float..?


            ## Yield line by line.
            for line in csv_reader:
                if ( not indices ):
                    #yield line
                    yield [ float( v ) for v in line ]
                else:
                    #yield [ line[ind] for ind in indices ]
                    yield [ float( line[ind] ) for ind in indices ]


    def get_csv_columns(self, fields=None):
        """
        Returns a dictionary holding the CSV values grouped into columns.

        Dict-keys correspond to |self.csv_header|, if |fields| is set only the specified columns are included.
        """

        ## TODO should we really use "get_..." for an I/O and computation intensive function..?

        if ( fields ):
            field_names = fields
        else:
            field_names = self.csv_header

        num_cols = len(field_names)


        ## Create a list for each column.
        cols = [ list() for i in range(num_cols) ]

        ## Read all csv lines and put the values in the corresponding columns,
        for line in self.get_csv_iterator(fields):
            for i in range(num_cols):
                cols[i].append( line[i] )


        ## Create output dictionary.
        ret = dict()
        for i in range(num_cols):
            ret[ field_names[i] ] = cols[i]

        return ret


    ## Convenience functions ##

    def get_json_header(self):
        return self.header

    def print_json_header(self):
        print( json.dumps(self.header, sort_keys=True, indent=4) )

    def get_csv_index_of(self, field_name):
        return self.csv_index[field_name]

    def get_csv_indices_of(self, field_names):
        return [ self.get_csv_index_of(name) for name in field_names ]

    # Specific getters:

    def get_general_header(self):
        return self.header["General"]

    def get_type(self):
        return self.header["General"]["Type"]

    def get_comment(self):
        comment = self.header["General"]["Comment"]
        if comment == None:
            comment = ""
            
        return comment

    def get_cpus(self):
        return self.header["ClassDefinitions"]["CPU"]["Siblings"]

    def get_nics(self):
        return self.header["ClassDefinitions"]["NIC"]["Siblings"]

    def get_sysinfo(self):
        return self.header["General"]["SystemInfo"]

    def get_hostname(self):
        try:
            return self.get_sysinfo()["hostname"]
        except KeyError:
            return "(unknown)"

    def get_environment(self):
        return self.header["General"]["Environment"]

    def get_human_readable_date(self):
        return self.header["General"]["Date"][0]

    def get_machine_readable_date(self):
        return self.header["General"]["Date"][1]


def get_cnl_files(directory):
    """
    Obtain all cpunetlog log files from the test result directory.

    :param directory: path to the directory that contains all test results
    :type directory: os.PathLike

    :return: list of paths to the cpunetlog log files
    :rtype: list(os.PathLike)
    """
    cnl_files = []

    dir_contents = os.listdir(directory)
    cnl_dirs = [d for d in dir_contents if d.startswith('h') and os.path.isdir(os.path.join(directory, d))]
    cnl_dirs = sorted(cnl_dirs)  # Sort cnl dirs such that the legend in the plot can easily show the correct host number

    for d in cnl_dirs:
        filename = [f for f in os.listdir(os.path.join(directory, d)) if f.endswith('cnl')][0]
        cnl_files.append(os.path.join(directory, d, filename))

    return cnl_files


def parse_cnl_file(filename, nic_fields = ["send", "receive"], nics=None):
    """
    Parse a CPUnetPLOT file and return a CNLParser instance, which can be used to retrieve the logged data.
    Copied from the original CPUnetPLOT.

    nics == None: Plot all nics and name them automatically
    nics == Dict( nic-name --> nic-label )
    """

    ## * Parse input file. *
    cnl_file = CNLParser(filename)

    ## Prepare data for matplotlib

    all_nics = cnl_file.get_nics()
    net_cols = list()
    net_labels = list()
    for nic_name in all_nics:
        try:
            if ( nics ):
                nic_label = nics[nic_name]  # NOTE: may fail, in this case we're not interested in this nic
            else:
                nic_label = nic_name

            for nic_field in nic_fields:
                    net_cols.append( nic_name + "." + nic_field )

                    # append "(send)" / "(receive)" (only) if both values are plotted
                    if ( len(nic_fields) > 1 ):
                        net_labels.append( "{} ({})".format(nic_label, nic_field) )
                    else:
                        net_labels.append( nic_label )
        except (KeyError):
            pass

    cpu_cols = [ cpu_name + ".util" for cpu_name in cnl_file.get_cpus() ]
    cpu_col_labels = [ cpu_name for cpu_name in cnl_file.get_cpus() ]

    cols = cnl_file.get_csv_columns()
    #x_values = cols["end"]

    ## Augment cnl_file with processed data.
    cnl_file.cols = cols
    cnl_file.net_col_names = net_cols
    cnl_file.net_col_labels = net_labels
    cnl_file.cpu_col_names = cpu_cols
    cnl_file.cpu_col_labels = cpu_col_labels
    #cnl_file.x_values = x_values

    return cnl_file


def get_throughput_data(cnl_files):
    """
    Parse the cpunetlog log files and return the throughput data.

    :param cnl_files: list of paths to the cpunetlog log files
    :type cnl_files: list(os.PathLike)

    :return: list of throughput data (one piece of data for each host). The data is composed of (timestamp, throughput) tuples
    :rtype: list(list(tuple(float, float)))
    """
    throughput_data = []

    for f in cnl_files:
        cur_host_throughput_data = []

        cnl = parse_cnl_file(f)

        first_timestamp = None
        for cnl_line in cnl.get_csv_iterator():  # Go through the lines of the log; the iterator yields each line as a list of values
            cur_timestamp = cnl_line[0]  # cf. cnl.csv_header: first value is the timestamp
            cur_throughput = cnl_line[-2]  # cf. cnl.csv_header: penultimate value is the outgoing throughput on the interface on which cnl recorded

            if first_timestamp is None:
                first_timestamp = cur_timestamp

            relative_timestamp = cur_timestamp - first_timestamp  # timestamps should start at 0 (e.g. for a 30s experiment: first timestamp: 0, last timestamp: ~30)

            cur_tuple = (relative_timestamp, cur_throughput)
            cur_host_throughput_data.append(cur_tuple)

        throughput_data.append(cur_host_throughput_data)

    return throughput_data


## MAIN ##
if __name__ == "__main__":

    ### DEMO:
    import sys

    filename = sys.argv[1]
    print( filename )

    ## * Parse input file. *
    cnl_file = CNLParser(filename)


    ## Display header informations.
    print( cnl_file.get_type() )
    print( json.dumps(cnl_file.get_json_header(), sort_keys=True, indent=4) )

    print( "CPUs: " + str(cnl_file.get_cpus()) )
    print( "NICs: " + str(cnl_file.get_nics()) )

    ## Display some csv/data fields.
    names = None
    names = ["eth0.send", "eth0.receive"]
    print( names )

    for x in cnl_file.get_csv_iterator(names):
        print( ", ".join(x) )
