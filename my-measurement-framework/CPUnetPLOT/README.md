# CPUnetPLOT
This folder contains files for creating plots from CPUnetLOG files.

- `cnl_plot_throughput.py`: Creates a throughput plot that contains the throughput of all hosts. Reads the contents of an experiment result directory that was created by `run_mininet.py` or `run_testbed.py` and parses the CPUnetLOG files in it.
- `cnl_plot_avg_throughput.py`: Creates a plot that presents for various buffer sizes the average throughput over several experiments.
- `cnl_plot_avg_throughput_and_jains_index.py`: Creates a plot that presents for various buffer sizes the average throughput of each flow as well as the average value of Jain's fairness index over several experiments.
