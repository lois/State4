#!/usr/bin/env python3

from cnl_library import get_cnl_files, get_throughput_data

import argparse
import os

import matplotlib.pyplot as plt


DEFAULT_OUTPUT_FILE_NAME = 'plot_jains_index.pdf'


def parse_args():
    """
    Parse command line arguments.
    """
    parser = argparse.ArgumentParser(description='Parses cpunetlog log files and plots Jain\'s fairness index.')

    parser.add_argument('directory', help='path to the directory that contains test results', nargs=1)
    parser.add_argument('-s', '--savefig', help='save the plot into a file. If a name is provided to this argument (i.e. "-s name.pdf"), it will be used as file name for the plot, ' + \
            'else (i.e. "-s") the name will be {}. Omit this option to display the plot directly'.format(DEFAULT_OUTPUT_FILE_NAME), nargs='?', const=DEFAULT_OUTPUT_FILE_NAME, default=None)

    args = parser.parse_args()
    return args


def calculate_jains_index(data):
    """
    Calculate Jain's fairness index for a given set of throughput values.

    :param data: A list containing the throughput values of each host at a given time
    :type data: list(float)

    :return: The value of Jain's fairness index
    :rtype: float
    """
    numerator = sum(data) ** 2
    denominator = len(data) * sum([x**2 for x in data])

    index = numerator / denominator
    return index


def plot_jain(throughput_data, savefig=None):
    """
    Use matplotlib to create a plot of Jain's fairness index.

    :param throughput_data: list of throughput data (one piece of data for each host)
    :type throughput_data: list(list(tuple(float, float)))

    :param savefig: The path to the file to store the plot into. If savefig is None, the plot will be displayed directly ("live")
    :type savefig: os.PathLike or None
    """
    # We assume that the throughput logs start at the same time and the logging interval is the same
    # Hence, it is safe to just cut off the end of one log if it contains more tuples than the other
    minimum_len = min([len(t) for t in throughput_data])
    cut_throughput_data = [t[:minimum_len] for t in throughput_data]

    x_values = [tup[0] for tup in cut_throughput_data[0]]  # Need only one host for x values (time)

    throughput_values = []
    for ct in cut_throughput_data:
        throughput_values.append([tup[1] for tup in ct])

    y_values = []
    for i in range(len(throughput_values[0])):
        cur_values = []
        for h in throughput_values:
            cur_values.append(h[i])

        index = calculate_jains_index(cur_values)
        y_values.append(index)

    plt.plot(x_values, y_values)

    plt.xlabel('Time [s]')
    plt.ylabel('Jain\'s Fairness Index')

    plt.grid(axis='x', b=False)
    plt.grid(axis='y', b=True)

    plt.tight_layout()

    if savefig:
        plt.savefig(savefig)
    else:
        plt.show()


def main():
    args = parse_args()

    directory = args.directory[0]
    if args.savefig is not None:
        savefig = os.path.join(directory, args.savefig)
    else:
        savefig = None

    cnl_files = get_cnl_files(directory)
    throughput_data = get_throughput_data(cnl_files)

    plot_jain(throughput_data, savefig)


if __name__ == '__main__':
    main()

