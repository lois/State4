# OtherPlots
This folder creates files for creating plots from sources other than tcplog and cpunetlog.

- `plot_buf_backlog.py`: Creates a buffer backlog plot from logged `tc` values.
