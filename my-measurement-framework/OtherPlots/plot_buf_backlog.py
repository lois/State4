#!/usr/bin/env python3

import argparse
import os

import matplotlib.pyplot as plt


DEFAULT_OUTPUT_FILE_NAME = 'plot_buffer-backlog.pdf'


def parse_args():
    """
    Parse command line arguments.
    """
    parser = argparse.ArgumentParser(description='Parse buffer backlog files and create a plot that shows the buffer backlog at the bottleneck (specifically, the number of packets buffered at the bottleneck).')

    parser.add_argument('directory', help='path to the directory that contains test results', nargs=1)
    parser.add_argument('-s', '--savefig', help='save the plot into a file. If a name is provided to this argument (i.e. "-s name.pdf"), it will be used as file name for the plot, ' + \
            'else (i.e. "-s") the name will be {}. Omit this option to display the plot directly'.format(DEFAULT_OUTPUT_FILE_NAME), nargs='?', const=DEFAULT_OUTPUT_FILE_NAME, default=None)

    parser.add_argument('-b', '--bytes', help='plot the backlog in bytes instead of packets', action='store_true')

    args = parser.parse_args()
    return args


def parse_backlog_bytes(bytes_str):
    if 'K' in bytes_str:
        kb_val = int(bytes_str.split('K')[0])
        bytes_val = kb_val * 1000
    elif 'M' in bytes_str:
        mb_val = int(bytes_str.split('M')[0])
        bytes_val = mb_val * 1000 * 1000
    else:
        bytes_val = int(bytes_str.split('b')[0])

    return bytes_val


def parse_buffer_backlog_file(buffer_backlog_file):
    """
    Parse a buffer backlog log file as created by run_mininet or run_testbed.

    :param buffer_backlog_file: The file created by the experiment that contains the output of tc -s qdisc show every 0.1s
    :type buffer_backlog_file: os.PathLike

    :return: A list of buffer backlog samples that contain the backlog in bytes and in packets. Specifically, this list contains a (bytes, packets) tuple for each sample
    :rtype: list((int, int))
    """
    samples = []

    lines = open(buffer_backlog_file, 'r').readlines()
    # Only check the backlog for "qdisc netem". That qdisc may have a parent, which has the same backlog values. We only need the values once.
    currently_parsing_qdisc_netem_info = False
    for line in lines:
        if line.startswith('qdisc netem'):
            currently_parsing_qdisc_netem_info = True
            continue
        elif line.startswith('qdisc '):  # Any qdisc other than netem, e.g. htb
            currently_parsing_qdisc_netem_info = False

        if not currently_parsing_qdisc_netem_info:
            continue

        stripped_line = line.strip()
        if stripped_line.startswith('backlog'):
            split = stripped_line.split()

            cur_bytes_str = split[1]
            cur_bytes = parse_backlog_bytes(cur_bytes_str)

            cur_packets_str = split[2]
            cur_packets = int(cur_packets_str.split('p')[0])

            cur_sample = (cur_bytes, cur_packets)
            samples.append(cur_sample)

    return samples


def plot_buffer_backlog(buffer_backlog_sample_list, savefig=None, plot_bytes=False):
    """
    Use matplotlib to create a buffer backlog plot.

    :param buffer_backlog_sample_list: A list of backlog samples as returned by parse_buffer_backlog_file()
    :type buffer_backlog_sample_list: list((int, int))
    :param savefig: The path to the file to store the plot into. If savefig is None, the plot will be displayed directly ("live")
    :type savefig: os.PathLike or None
    :param plot_bytes: Whether to plot the backlog in bytes instead of packets
    :type plot_bytes: bool
    """
    x_values = [i*0.1 for i in range(len(buffer_backlog_sample_list))]  # One sample every 0.1s

    sample_index = 0 if plot_bytes else 1
    y_values = [sample[sample_index] for sample in buffer_backlog_sample_list]

    plt.plot(x_values, y_values)

    plt.xlabel('Time [s]')
    yunit = 'Bytes' if plot_bytes else 'packets'
    plt.ylabel('Buffer Backlog [{}]'.format(yunit))

    plt.grid(axis='x', b=False)
    plt.grid(axis='y', b=True)

    plt.tight_layout()

    if savefig:
        plt.savefig(savefig)
    else:
        plt.show()


def main():
    args = parse_args()

    directory = args.directory[0]
    if args.savefig is not None:
        savefig = os.path.join(directory, args.savefig)
    else:
        savefig = None

    plot_bytes = True if args.bytes else False

    buffer_backlog_file = os.path.join(directory, 'buffer-backlog.log')
    buffer_backlog_sample_list = parse_buffer_backlog_file(buffer_backlog_file)

    plot_buffer_backlog(buffer_backlog_sample_list, savefig, plot_bytes)


if __name__ == '__main__':
    main()

