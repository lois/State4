import argparse
import subprocess
import os


def get_script(data):
    """
    Get the script that can be used for plotting 'data', e.g. for cwnd, use TCPplot/tl_plot_cwnd.py

    :param data: The type of data; this can be 'cwnd', 'rtt', and 'throughput'
    :type data: str
    """
    if data == 'cwnd':
        return 'TCPplot/tl_plot_cwnd.py'
    elif data == 'rtt':
        return 'TCPplot/tl_plot_rtt.py'
    elif data == 'throughput':
        return 'CPUnetPLOT/cnl_plot_throughput.py'
    elif data == 'buf-backlog':
        return 'OtherPlots/plot_buf_backlog.py'
    else:
        raise Exception('Invalid data type {}!'.format(data))


def plot(data, directory, args=[]):
    """
    Plot a certain type of data.

    :param data: What to plot; this can be 'cwnd', 'rtt', and 'throughput'
    :type data: str
    :param directory: The directory in which the test results (and thus the log files) are located
    :type directory: os.PathLike
    :param args: a list of arguments that should be passed to the script
    :type args: list(str)
    """
    print('Plotting {}'.format(data))
    
    base = os.getcwd()
    script = get_script(data)
    script = base + script
    cmd = [script, directory, '-s']
    for arg in args:
        cmd.append(arg)
    print(cmd)
    subprocess.run(cmd)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d --directories', dest='directories',
                        default=['.'], nargs='+', help='Paths to the directories that contain experiment results (default: .)')
    
    args = parser.parse_args()

    paths = args.directories
    print('Found {} valid sub directories.'.format(len(paths)))

    paths = sorted(paths)

    for i, directory in enumerate(paths):
        print('{}/{} Processing {}'.format(i + 1, len(paths), directory))
        
        plot('cwnd', directory)
        plot('rtt', directory, args=['-l'])
        plot('throughput', directory)


if __name__ == "__main__":
    try:
        main()
    except OSError as e:
        print(e)
