# from http import server
import py_compile
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.link import TCLink
from mininet.log import setLogLevel
from mininet.cli import CLI
from mininet.clean import cleanup

from helper.util import print_error, print_warning
from helper.util import parse_link_commands, parse_host_commands, parseConfigFile, store_experiment_configuration, parse_rtt
from helper.util import sleep_progress_bar

from helper import TEXT_WIDTH

import os
import sys
import subprocess
import time
import argparse
import re
import glob
import math

def parse_args():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()


    parser.add_argument('-d', dest='directory',
                        default='test/', help='Path to the output directory. (default: test/)')
    parser.add_argument('-n', dest='name',
                        help='Name of the output directory. (default: <config file name>)')

    args = parser.parse_args()
    return args


def run_state_test(net,name, interval, duration):
    """
    Runs the following test
    |---------------------------------|
    0  2      14  15  16             30
       1P     RS  2P  RE
    1P --- h5 start port-knocking
    2P --- h6 start port-knocking
    RS --- Reconfiguration starts
    RE --- Reconfiguration ends
    Note: 
        Port-knocking sequence takes three seconds to complete
        Reconfiguration now takes around three seconds to complete

        3 reading hosts ping per sec
        
    """
    print('-' * TEXT_WIDTH)
    print('Starting test: {}'.format(name))
    print('Total duration: {}s'.format(duration))

    time.sleep(1) # 1 sec sleep before start
    
    cmd = 'timeout {} ping -i {} {} &' # timeout interval dst_addr

    # h2 being the receiving server
    server_addr = "10.0.2.2"
    begin = time.time()
    host = net.get('h{}'.format(2))
    print("INFO h2: "+"./client/read.sh " + str(duration) + " &")
    host.cmd("./client/read.sh " + str(duration) + " &")

    for i in range(1,2):
        host = net.get('h{}'.format(i))
        print('h{}'.format(i) + ': ' +  cmd.format(duration, interval, server_addr))
        host.cmd(cmd.format(duration, interval, server_addr))
    print('h2 recevied packets starts, h1 ping started! ')
    
    # h4 knock first
    pk_host1 = net.get('h4') 
    pk_host2 = net.get('h5')
    start = time.time()
    pk_host1.cmd('python3 ./client/knock.py &')
    pk_host2.cmd('python3 ./client/knock.py ')

    end = time.time()
    knock_time = end - start
    print("INFO: h4 and h5 knocking time taken = {}".format(str(knock_time)))
    pk_host1.cmd('timeout {} ping -i {} {} &'.format(math.floor(duration - knock_time), interval, server_addr))
    pk_host2.cmd('timeout {} ping -i {} {} &'.format(math.floor(duration - knock_time), interval, server_addr))

    print("INFO: h4 and h5 initiate ping for {} seconds".format(math.floor(duration - knock_time)))
    print("INFO: Now sleep for {} seconds until 14 seconds".format(math.floor(14 - knock_time)))
    time.sleep(14 - knock_time)
    print("INFO: Reminder -> ****** Start reconfiguration ******")
    time.sleep(1) # start the second knocking around 
    print("15s reached, starting port-knocking for h6")
    pk_host = net.get('h6')
    start = time.time()
    pk_host.cmd('python3 ./client/knock.py')
    end = time.time()
    knock_time = end - start
    print("h6 knocking time taken = {}".format(str(knock_time)))
    pk_host.cmd('timeout {} ping -i {} {} &'.format(math.floor(duration - (15 + knock_time)), interval, server_addr))
    print("h6 initiate ping for {} seconds".format(math.floor(duration -(15 + knock_time))))

    complete = duration 
    current_time = 15 + knock_time

    current_time = sleep_progress_bar((complete - current_time) % 1, current_time=current_time, complete=complete)
    current_time = sleep_progress_bar(complete - current_time, current_time=current_time, complete=complete)
    print('-' * TEXT_WIDTH)    

    finish = time.time()
    print("Test time = {}".format(str(finish - begin)))

def run_single_flow_test(net, name, output_directory, duration):
    """
    Runs test with a single flow to test the state divergence
    """
    complete = duration
    start = 0
    current_time = 0
    recv = net.get('h{}'.format(2))
    report_interval = 0.1
    host = net.get('h1')
    port_num = 1001

    print('-' * TEXT_WIDTH)
    print('Starting test: {}'.format(name))
    print('Total duration: {}s'.format(duration))

    time.sleep(5)
    
    print('h2: timeout {} ncat --keep-open -m 10 -l -p 9000 > /dev/null &'.format(duration))
    recv.cmd('timeout {} iperf -s -t {} -p {} > /dev/null &'.format(duration, duration,str(1001)))

    print('h1: tcplog --hard-timeout {} -o file -w {}/tcplog-h1.log -q &'.format(duration,output_directory))
    print('h1: cpunetlog -l --path {} -q -i {} &'.format(os.path.join(output_directory, 'h1'), str(report_interval)))
    host.cmd('tcplog -r {} --hard-timeout {} -o file -w {}/tcplog-h1.log -q &'.format(str(report_interval),duration,output_directory))
    host.cmd('cpunetlog -l --path {} -q -i {} &'.format(os.path.join(output_directory, 'h{}'.format(1)),str(report_interval)))

    current_time = sleep_progress_bar(start, current_time=current_time, complete=complete)
    log_String = '  h{}: {} -> {}'.format(1, host.IP(), recv.IP())

    print('h{}: timeout {} -t {} -c {} -p {} &'.format(1, duration, duration, recv.IP(),str(port_num)))
    host.cmd('timeout {} iperf -t {} -c {} -p {} &'.format(duration,duration,recv.IP(),str(port_num)))

    print(log_String + ' ' * (TEXT_WIDTH - len(log_String)))

    current_time = sleep_progress_bar((complete - current_time) % 1, current_time=current_time, complete=complete)
    current_time = sleep_progress_bar(complete - current_time, current_time=current_time, complete=complete)
    
    return output_directory


# deprecated testing code
def run_test(net, name, output_directory, duration):
    """ 
    Runs the whole experiment.
    """
    print('-' * TEXT_WIDTH)
    print('Starting test: {}'.format(name))
    print('Total duration: {}s'.format(duration))

    time.sleep(1)


    recv = net.get('h{}'.format(2))
    port_num = 1001
    # TODO: investigate on the specific amount of data to transmit
    print('h2: timeout {} ncat --keep-open -m 10 -l -p 9000 > /dev/null &'.format(duration))
    recv.cmd('timeout {} iperf -s -t 30 -p {} > /dev/null &'.format(duration, str(1001)))
    recv.cmd('timeout {} iperf -s -t 30 -p {} > /dev/null &'.format(duration, str(1002)))
    recv.cmd('timeout {} iperf -s -t 30 -p {} > /dev/null &'.format(duration, str(1003)))
    recv.cmd('timeout {} iperf -s -t 30 -p {} > /dev/null &'.format(duration, str(1004)))
    recv.cmd('timeout {} iperf -s -t 30 -p {} > /dev/null &'.format(duration, str(1005)))

    report_interval = 0.5

    for i in range(1,7):
        
        if i == 2: 
            continue

        host = net.get('h{}'.format(i))
        duration = 30
        print('h{}: tcplog --hard-timeout {} -o file -w {}/tcplog-h{}.log -q &'.format(i,duration,output_directory, i))
        print('h{}: cpunetlog -l --path {} -q -i {} &'.format(i,os.path.join(output_directory, 'h{}'.format(i)), str(report_interval)))
        host.cmd('tcplog -r {} --hard-timeout {} -o file -w {}/tcplog-h{}.log -q &'.format(str(report_interval),duration,output_directory, i))
        host.cmd('cpunetlog -l --path {} -q -i {} &'.format(os.path.join(output_directory, 'h{}'.format(i)),str(report_interval)))

    complete = duration
    current_time = 0

    try:


        for host_counter in range(1,7):
            if(host_counter == 2): 
                continue
            current_time = sleep_progress_bar(0, current_time=current_time, complete=complete)
            send = net.get('h{}'.format(host_counter))

            log_String = '  h{}: {} -> {}'.format(host_counter, send.IP(), recv.IP())

            print('h{}: timeout {} -t 30 -c {} -p {} &'.format(host_counter, duration,recv.IP(),str(port_num)))
            send.cmd('timeout {} iperf -t 30 -c {} -p {} &'.format(duration,recv.IP(),str(port_num)))
            port_num += 1
            print(log_String + ' ' * (TEXT_WIDTH - len(log_String)))

        current_time = sleep_progress_bar((complete - current_time) % 1, current_time=current_time, complete=complete)
        current_time = sleep_progress_bar(complete - current_time, current_time=current_time, complete=complete)
    except (KeyboardInterrupt, Exception) as e:
        if isinstance(e, KeyboardInterrupt):
            print_warning('\nReceived keyboard interrupt. Stop Mininet.')
        else:
            print_error(e)


    print('-' * TEXT_WIDTH)


