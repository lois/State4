# TCP Evaluation Framework

This repository contains frameworks for running experiments with TCP, and logging and plotting some information related to congestion control. These experiment can be conducted using mininet (`run_mininet.py`) or on a testbed (`run_testbed.py`).

Parts of the mininet framework in this repository are copied from https://gitlab.lrz.de/tcp-bbr/measurement-framework ([paper](https://www.net.in.tum.de/fileadmin/bibtex/publications/papers/IFIP-Networking-2018-TCP-BBR.pdf)). Parts of the CPUnetLog and TCPlog utilities are from https://git.scc.kit.edu/CPUnetLOG.

## Contents
This repository contains the following files:

- `run_mininet.py`: Script for running mininet experiments (more information below)
- `run_testbed.py`: Script for running testbed experiments (more information below)
- `analyze.py`: Script for creating plots for an experiment (more information below)
- `sync.py`: Script that synchronizes test results between the server on which the tests were run and your local machine. You should run this script on your local, and it will automatically copy all unsychronized results from the server. It also creates a nicer directory structure that is easier to work with than the structure created by the experiment runner scripts on the server

- `dirname_structure.txt`: Text file that explains the structure of the result directory (as created by `sync.py`) and how you should name config files

- `configs/`: Directory to store your config files into. The `configs/` directory contains a few example config files; all config files can be found in `configs/0/`
- `CPUnetPLOT/`, `TCPplot/`, and `OtherPlots`: Contain scripts for creating plots. The analyze script simply calls these scripts
- `helper/`: Contains utility functions for the experiment runner scripts

- `testbed-info.txt`: Information about the testbed as well as instructions on how to set it up for experiments; there is an old and a new testbed. The info file describes both. `new-testbed-setup.pdf` illustrates how the servers of the new testbed are connected
- `testbed_setup/`: Contains scripts for automatically configuring the new testbed for experiments after booting the servers

## Requirements
- Python 3
- mininet (we are using version 2.3.0)
- matplotlib (note that you may have to install it as root (`sudo pip3 install matplotlib`) since (for mininet) you probably need to execute the scripts as root, and the root user's python environment differs from your own user's)
- netcat
- [CPUnetLOG](https://git.scc.kit.edu/CPUnetLOG/CPUnetLOG)
- [TCPlog](https://git.scc.kit.edu/CPUnetLOG/TCPlog)
- make, libelf-dev, flex, bison (for building the kernel)

Both experiment runner scripts use CPUnetLOG and TCPlog for logging TCP information. Hence, when running a mininet experiment, these tools must be installed on the host machine, and when running a testbed experiment, they must be installed on the machines that are used as senders. Similarly, both scripts use netcat for creating a TCP stream. Hence, for mininet, netcat must be installed on the host machine, and for testbed, netcat must be installed on the machines that are used as senders and on the machine that is used as receiver.

## Experiment Framework
You can run mininet experiments using `run_mininet.py`. You can run testbed experiments using `run_testbed.py`. Both will store log files into a results directory (by default: `test/` for mininet experiments and `testbed/` for testbed experiments). You can then use `analyze.py` to create plots.

### Mininet
The mininet script sets up a mininet topology that corresponds to your config file and runs the experiment. See below for more information on the config file.

The execution of the mininet script requires root privileges (since mininet requires root privileges).

```bash
usage: run_mininet.py [-h] [-d DIRECTORY] [-n NAME] CONFIG

positional arguments:
  CONFIG        Path to the config file.

optional arguments:
  -h, --help    show this help message and exit
  -d DIRECTORY  Path to the output directory. (default: test/)
  -n NAME       Name of the output directory. (default: <config file name>)
```

### Testbed
The testbed script runs an experiment on actual servers. By default, these servers are `{tb31, tb33, tb35, tb34}.tm.kit.edu`. The script can be run from another machine. However, it is currently meant to be run from tb31 (which is why tb31's IP address in the script is 127.0.0.1). The script uses ssh to connect to each server and run commands remotely. It configures the servers corresponding to your config file and runs the experiment. Thereafter, it cleans up the servers by removing any configurations done for the experiment. See below for more information on the config file.

```bash
usage: run_testbed.py [-h] [-n NAME] [-d DIRECTORY] [-c] [config]

Runs experiments on the testbed, i.e. servers tb31, tb33, tb35, and tb34.

positional arguments:
  config                path to the config file that defines experiment variables

optional arguments:
  -h, --help            show this help message and exit
  -n NAME, --name NAME  name of the result directory (default: <name of config file>
  -d DIRECTORY, --directory DIRECTORY
                        path to the output directory in which the result directory is (default: testbed/)
  -c, --cleanup         only run cleanup, i.e. reset all tc netem and sysctl configurations on the testbed servers
```

The file `testbed-info.txt` contains information on how the testbed is set up, and how to configure it for experiments.

### Config File
The configuration file is a text file formatted as follows:

```
host, <algorithm>, <rtt>, <start>, <stop>
link, <type>, <value>, <start>
```

A line that starts with 'host' adds a new TCP stream, which uses 'algorithm' for congestion control, has RTT 'rtt', starts at 'start' seconds, and ends 'stop' seconds later. 'algorithm' must be specified as string that corresponds to the name of the congestion control in the Linux kernel, e.g. 'cubic' or 'bbr2', 'rtt' must be specified in ms, e.g. '42ms', and 'start' and 'stop' as integers (numbers in seconds), e.g. '0' and '60'. Note that 'start' values are relative to the previous 'start' value, that is, if you want 4 flows to start 0s, 2s, 2s, and 4s after the start of the experiment, respectively, you must use 'start' values of 0, 2, 0, and 2.

A line that starts with 'link' configures the bottleneck link. The 'type' can be bw for bandwidth or bufsize for buffer size; 'value' is the value to set for 'type'. The change gets applied after 'start' seconds. For bw, 'value' must be specified in mbit, e.g. '10mbit'. For bufsize, 'value' must be specified in either packets or BDPs, e.g. '20packets' or '1bdp'. If you specify the bufsize value in BDPs, you must specify it after the bw, that is, the line that defines the bufsize must come after the line that defines the bw. It is recommended to specify the buffer size in packet because that is more precise and clear than bdp. 'start' must be specified as integer (number in seconds), e.g. '0'.
NOTE: Currently, only 'start' = 0 is supported for link commands. You cannot change the bottleneck link characteristics in the middle of the experiment.

All host commands (i.e. lines that start with 'host') should come before the link commands (i.e. lines that start with 'link'). 

This is an example config file:
```
host, bbr, 40ms, 0, 40
host, cubic, 50ms, 5, 30
link, bw, 5mbit, 0
link, bufsize, 42packets, 0
```
This config file results in the following test
```
0s  Set bottleneck bw to 5 mbit/s, and buffer size to 42 packets
0s  |   bbr flow, 40ms
    |        |
    |        |    
5s  |        |        cubic flow, 50ms
    |        |               |
    |        |               |
    |        |               |
    |        |               |
    |        |               |
    |        |               |
    |        |               |
    |        |               |
    |        |               |
    |        |               |
    |        |               |
    |        |               |
35s |        |             stop
    |        |
40s |      stop
```

### Analysis 
The analysis script is called after the execution of the experiment and creates plots.

The experiment runner scripts store log files into a result directory. The analysis script calls plotting scripts in CPUnetPLOT/ and TCPplot/ that use these log files for creating the plots.

Note that when running experiments with mininet, the result directory will be created as root. Thus, for mininet experiments, you must call `analyze.py` with root priviliges as well (or adjust the directory permissions beforehand). 
```bash
usage: analyze.py [-h] [-s {pcap,csv}] [-o {pdf+csv,pdf,csv}]
                        [-p1 PCAP1] [-p2 PCAP2] [-t DELTA_T] [-r] [-n]
                        PATH

positional arguments:
  PATH                  path to the working directory

optional arguments:
  -h, --help            show this help message and exit
  -s {pcap,csv}         Create plots from csv or pcap
  -o {pdf+csv,pdf,csv}  Output Format (default: pdf+csv)
  -p1 PCAP1             Filename of the pcap before the bottleneck (default:
                        s1.pcap)
  -p2 PCAP2             Filename of the pcap behind the bottleneck (default:
                        s3.pcap)
  -t DELTA_T            Interval in seconds for computing average
                        throughput,... (default: 0.2)
  -r                    Process all sub-directories recursively.
  -n                    Only process new (unprocessed) directories.
```
