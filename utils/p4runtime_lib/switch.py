# Copyright 2017-present Open Networking Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from functools import wraps
from atexit import register
# from hmac import digest
import logging
import re
# from Queue import Queue
import sys
if sys.version_info < (3,0):
    import Queue as queue 
else:
    import queue
from abc import abstractmethod
from datetime import datetime

# import queue
import grpc
import threading
from p4.v1 import p4runtime_pb2
from p4.v1 import p4runtime_pb2_grpc
from p4.tmp import p4config_pb2

MSG_LOG_MAX_LEN = 1024

# List of all active connections
connections = []

class P4RuntimeException(Exception):
    def __init__(self, grpc_error):
        super().__init__()
        self.grpc_error = grpc_error
    
    def __str__(self):
        message = "P4Runtime RPC error ({}): {}".format(
            self.grpc_error.code().name, self.grpc_error.details())
        return message

def ShutdownAllSwitchConnections():
    for c in connections:
        c.shutdown()

class SwitchConnection(object):

    def __init__(self, name=None, address='127.0.0.1:50051', device_id=0,
                 proto_dump_file=None):
        self.name = name
        self.address = address
        self.device_id = device_id
        self.p4info = None
        self.channel = grpc.insecure_channel(self.address)
        if proto_dump_file is not None:
            interceptor = GrpcRequestLogger(proto_dump_file)
            self.channel = grpc.intercept_channel(self.channel, interceptor)
        self.client_stub = p4runtime_pb2_grpc.P4RuntimeStub(self.channel) # runtime stub
        self.requests_stream = IterableQueue()
        self.stream_msg_resp = self.client_stub.StreamChannel(iter(self.requests_stream))
        self.role_name = None
        self.proto_dump_file = proto_dump_file
        self.stream_out_queue = None
        self.stream_in_queue = None

        self.stream_out_queue = IterableQueue()
        self.stream_in_queue = {
            'arbitration': IterableQueue(),
            'packet': IterableQueue(),
            'digest': IterableQueue(),
            'idle_timeout_notification': IterableQueue(),
            'unknown': IterableQueue()
        }
        connections.append(self)

    def parse_p4runtime_error(f):
        @wraps(f)
        def handle(*args, **kwargs):
            try:
                return f(*args,**kwargs)
            except grpc.RpcError as e:
                raise P4RuntimeException(e) 
        return handle    
        

    @parse_p4runtime_error
    def stream_receive(self,stream):
        for packet in stream:
            if(packet.HasField('arbitration')):
                self.stream_in_queue['arbitration'].put(packet)
            elif(packet.HasField('packet')):
                self.stream_in_queue['packet'].put(packet)
            elif(packet.HasField('digest')):
                self.stream_in_queue['digest'].put(packet)

    def stream_receive_wrapper(self, stream):
        try:
            self.stream_receive(stream=stream)
        except P4RuntimeException as e:
            logging.critical('Stream error, closing')
            logging.critical(e)

    def getStreamPacket(self,type_,timeout=5):
        # if type_ not in self.stream_msg_resp:
        #     print("Unknown stream type '{}'".format(type_))
        #     return None
            # message = self.requests_stream.get(timeout=timeout)
        try:
            for x in self.stream_msg_resp:
                return x
        except queue.Empty:
            return None
            # print(message)
            # return message



    def handshake(self):
        request = p4runtime_pb2.StreamMessageRequest()
        request.arbitration.device_id = self.device_id
        request.arbitration.election_id.high = 0
        request.arbitration.election_id.low = 1
        if(self.role_name is not None):
            request.arbitration.role_name = self.role_name
        self.stream_out_queue.put(request)

        response = self.getStreamPacket('arbitration',timeout=2) # 2 for a temp place-holder val

        if response is None:
            print("Failed to establish session with ")
        else:
            print(response)
        # can check response here

    def listenToCPUPort(self):
        if(self.stream_out_queue is not None or self.stream_in_queue is not None):
            sys.err.write("Error: listenToCPUPort() method already called once previously")
            sys.exit(255)

        self.channel = grpc.insecure_channel(self.address)
        self.stub = p4runtime_pb2_grpc.P4RuntimeStub(self.channel)
        self.stream = self.stub.StreamChannel(IterableQueue())
        # self.stream_receive_thread = threading.Thread(
        #     target=self.stream_receive_wrapper, args=(iter(self.resp),)
        # )
        # self.stream_receive_thread.start()
        # self.stream_msg_resp
        # # self.handshake()

    @abstractmethod
    def buildDeviceConfig(self, **kwargs):
        return p4config_pb2.P4DeviceConfig()

    def shutdown(self):
        self.requests_stream.close()
        self.stream_msg_resp.cancel()

    def MasterArbitrationUpdate(self, dry_run=False, **kwargs):
        request = p4runtime_pb2.StreamMessageRequest()
        request.arbitration.device_id = self.device_id
        request.arbitration.election_id.high = 0
        request.arbitration.election_id.low = 1

        if dry_run:
            print ("P4Runtime MasterArbitrationUpdate: ", request)
        else:
            self.requests_stream.put(request)
            for item in self.stream_msg_resp:
                return item # just one

    #NOTE: RECONCILE_AND_COMMIT sees to be not working yet!
    def VerifyAndSaveForwardingPipelineConfig(self, p4info, dry_run=False, **kwargs):
        device_config = self.buildDeviceConfig(**kwargs)
        request = p4runtime_pb2.SetForwardingPipelineConfigRequest()
        # request.Action
        request.election_id.low = 1
        request.device_id = self.device_id
        config = request.config

        
        config.p4info.CopyFrom(p4info)
        config.p4_device_config = device_config.SerializeToString()

        request.action = p4runtime_pb2.SetForwardingPipelineConfigRequest.VERIFY_AND_SAVE
        
        if dry_run:
            print ("P4Runtime VerifyAndSaveForwardingPipelineConfig:", request)
        else:
            self.client_stub.SetForwardingPipelineConfig(request)


    def CommitForwardingPipelineConfig(self, p4info, dry_run=False, **kwargs):
        device_config = self.buildDeviceConfig(**kwargs)
        request = p4runtime_pb2.SetForwardingPipelineConfigRequest()

        request.election_id.low = 1
        request.device_id = self.device_id
        config = request.config

        request.action = p4runtime_pb2.SetForwardingPipelineConfigRequest.COMMIT

        if dry_run:
            print ("P4Runtime CommitForwardingPipelineConfig:", request)
        else:
            self.client_stub.SetForwardingPipelineConfig(request)


    def SetForwardingPipelineConfig(self, p4info, dry_run=False, action = p4runtime_pb2.SetForwardingPipelineConfigRequest.VERIFY_AND_COMMIT, **kwargs):

        device_config = self.buildDeviceConfig(**kwargs)
        request = p4runtime_pb2.SetForwardingPipelineConfigRequest()
        # request.Action
        request.election_id.low = 1
        request.device_id = self.device_id
        config = request.config

        config.p4info.CopyFrom(p4info)
        config.p4_device_config = device_config.SerializeToString()

        request.action = action
        if dry_run:
            print ("P4Runtime SetForwardingPipelineConfig:", request)
        else:
            self.client_stub.SetForwardingPipelineConfig(request)

    def GetForwardingPipelineConfig(self, dry_run=False, **kwargs):
        #TODO: this funcion hasn't been finished yet!
        # device_config = self.buildDeviceConfig(**kwargs)
        print("P4runtime_pb2 file location: " + p4runtime_pb2.__file__)
        request = p4runtime_pb2.GetForwardingPipelineConfigRequest()
        # request.election_id.low = 1
        request.device_id = self.device_id
        #request.ResponseType = 1
        # request.action = p4runtime_pb2.GetForwardingPipelineConfigRequest.VERIFY_AND_COMMIT
        if dry_run:
            print ("P4Runtime GetForwardingPipelineConfig:", request)
        else:
            # self.client_stub.GetForwardingPipelineConfig(request)
            response = self.client_stub.GetForwardingPipelineConfig(request)
            print(type(response))
            print(response.config.p4info)
            with open('out.txt','w') as f:
                
                f.write(response.config.p4_device_config)
            

    def WriteTableEntry(self, table_entry, dry_run=False):
        request = p4runtime_pb2.WriteRequest()
        request.device_id = self.device_id
        request.election_id.low = 1
        update = request.updates.add()
        if table_entry.is_default_action:
            update.type = p4runtime_pb2.Update.MODIFY
        else:
            update.type = p4runtime_pb2.Update.INSERT
        update.entity.table_entry.CopyFrom(table_entry)
        if dry_run:
            print ("P4Runtime Write:", request)
        else:
            self.client_stub.Write(request)

    def ReadTableEntries(self, table_id=None, dry_run=False):
        request = p4runtime_pb2.ReadRequest()
        request.device_id = self.device_id
        entity = request.entities.add()
        table_entry = entity.table_entry
        if table_id is not None:
            table_entry.table_id = table_id
        else:
            table_entry.table_id = 0
        if dry_run:
            print ("P4Runtime Read:", request)
        else:
            for response in self.client_stub.Read(request):
                yield response

    def ReadCounters(self, counter_id=None, index=None, dry_run=False):
        request = p4runtime_pb2.ReadRequest()
        request.device_id = self.device_id
        entity = request.entities.add()
        counter_entry = entity.counter_entry
        if counter_id is not None:
            counter_entry.counter_id = counter_id
        else:
            counter_entry.counter_id = 0
        if index is not None:
            counter_entry.index.index = index
        if dry_run:
            print ("P4Runtime Read:", request)
        else:
            for response in self.client_stub.Read(request):
                yield response

    # Finish such function for reading the registers

    def ReadRegister(self, register_id=None, index=0, dry_run=False):
        request = p4runtime_pb2.ReadRequest()
        # register id: 372428421

        request.device_id = self.device_id
        entity = request.entities.add()
        reg_entry = entity.register_entry
        
        if register_id is not None:
            reg_entry.register_id = register_id
        # leaving the index unset will use wild-card read
        # if (index != 0):
        #     reg_entry.index = index
        # do i need to set data?
        
        if dry_run:
            print ("P4Runtime Read: ", request)
        else:
            for response in self.client_stub.Read(request):
                print(response)
                #yield response

    def WritePREEntry(self, pre_entry, dry_run=False):
        request = p4runtime_pb2.WriteRequest()
        request.device_id = self.device_id
        request.election_id.low = 1
        update = request.updates.add()
        update.type = p4runtime_pb2.Update.INSERT
        update.entity.packet_replication_engine_entry.CopyFrom(pre_entry)
        if dry_run:
            print ("P4Runtime Write:", request)
        else:
            self.client_stub.Write(request)

class GrpcRequestLogger(grpc.UnaryUnaryClientInterceptor,
                        grpc.UnaryStreamClientInterceptor):
    """Implementation of a gRPC interceptor that logs request to a file"""

    def __init__(self, log_file):
        self.log_file = log_file
        with open(self.log_file, 'w') as f:
            # Clear content if it exists.
            f.write("")

    def log_message(self, method_name, body):
        with open(self.log_file, 'a') as f:
            ts = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            msg = str(body)
            f.write("\n[%s] %s\n---\n" % (ts, method_name))
            if len(msg) < MSG_LOG_MAX_LEN:
                f.write(str(body))
            else:
                f.write("Message too long (%d bytes)! Skipping log...\n" % len(msg))
            f.write('---\n')

    def intercept_unary_unary(self, continuation, client_call_details, request):
        self.log_message(client_call_details.method, request)
        return continuation(client_call_details, request)

    def intercept_unary_stream(self, continuation, client_call_details, request):
        self.log_message(client_call_details.method, request)
        return continuation(client_call_details, request)

class IterableQueue(queue.Queue):
    _sentinel = object()

    def __iter__(self):
        return iter(self.get, self._sentinel)

    def close(self):
        self.put(self._sentinel)
