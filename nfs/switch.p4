// Current: HULA  + P4Knocking

// HULA Source: https://github.com/rachitnigam/Hula-hoop
// Port-knocking firewall: https://github.com/ederollora/NETPROC_P4Knocking Implemntation_1
// L2-forwarding: Implemented by myself 

#include "includes/defines.p4"

/* -*- P4_16 -*- */
#include <core.p4>
#include <v1model.p4>

/* My flow counter typedef */
typedef bit<80> PacketByteCount_t;
typedef bit<32> PacketCounter_t;

/* Port-knocking typedef */
typedef bit<4> pk_stage_t;
typedef bit<80> reg_key;
typedef bit<16> l4_port_t;
typedef bit<16> index_t;
typedef bit<16> half_ipAddr_t;
typedef bit<16> id_t;


/* HULA typedef */
typedef bit<9>  egressSpec_t;
typedef bit<48> macAddr_t;
typedef bit<32> ip4Addr_t;
typedef bit<9> port_id_t;
typedef bit<8> util_t;
typedef bit<24> tor_id_t;
typedef bit<48> time_t;

/* MAX DEFINE */

/* Constants about the topology and switches. */
const port_id_t NUM_PORTS = 255;
const tor_id_t NUM_TORS = 512;
const bit<32> EGDE_HOSTS = 4;

/* Declaration for the various packet types. */ 
const bit<16> TYPE_IPV4 = 0x800;
const bit<8> PROTO_UDP = 0x11;
const bit<8> PROTO_ICMP = 0x01;
const bit<8> PROTO_HULA = 0x42;
const bit<8> PROTO_TCP = 0x06;
const bit<9> TCP_SYN = 0x2;
/* adding port-knocking consts */
const bit<4> BYTE_TO_BIT = 8;
const port_id_t CPU_PORT = 255; // same as NUM_PORTS


/* Tracking things for flowlets */
const time_t FLOWLET_TOUT = 48w1 << 3;
const util_t PROBE_FREQ_FACTOR = 6;
const time_t KEEP_ALIVE_THRESH = 48w1 << PROBE_FREQ_FACTOR;
const time_t PROBE_FREQ = 48w1 << PROBE_FREQ_FACTOR; // Here for documentation. Unused.

/*************************************************************************
*********************** H E A D E R S  ***********************************
*************************************************************************/
const port_id_t DROP_PORT = (port_id_t)4w0xF;
// const port_id_t PSA_PORT_RECIRCULATE = (port_id_t) 0xfffffffa;

header hula_t {
    bit<24> dst_tor;
    bit<8> path_util;
}

header ethernet_t {
    macAddr_t dstAddr;
    macAddr_t srcAddr;
    bit<16>   etherType;
}

header ipv4_t {
    bit<4>    version;
    bit<4>    ihl;
    bit<8>    diffserv;
    bit<16>   totalLen;
    bit<16>   identification;
    bit<3>    flags;
    bit<13>   fragOffset;
    bit<8>    ttl;
    bit<8>    protocol;
    bit<16>   hdrChecksum;
    ip4Addr_t srcAddr;
    ip4Addr_t dstAddr;
}

header icmp_t {
    bit<8> tp;
    bit<8> code;
    bit<16> chk;
    bit<16> id;
    bit<16> seqNum;
}

header tcp_t {
  bit<16> srcPort;
  bit<16> dstPort;
  bit<32> seq;
  bit<32> ack;
  bit<4> dataofs;
  bit<3> reserved;
  bit<9> flags;
  bit<16> window;
  bit<16> chksum;
  bit<16> urgptr;
}

header udp_t {
    bit<16> srcPort;
    bit<16> dstPort;
    bit<16> length_;
    bit<16> checksum;
}
struct pk_metadata_t {
    pk_stage_t stage;
    bit<16> id;
    bool direct_fwd;
    bool allowed;
    bool has_id;
    bool protected_port;
}
struct packetIn_metadata{ 
    bit<8> reg_num; 
    bit<24> index_num; 
    bit<80> value;
}


struct metadata {
    @field_list(0)
	packetIn_metadata pkIn_metadata_1;

    bit<2> num_state_changes;
    bit<9> nxt_hop;
    bit<32> self_id;
    bit<32> dst_tor;
    pk_metadata_t pk_metadata;
    bit<80> temp_count;
}

@controller_header("packet_in")
header packet_in_header_t{ 
    bit<8> reg_num; 
    bit<24> index_num; 
    bit<80> value;
}


struct headers {
    packet_in_header_t packet_in;
    ethernet_t   ethernet;
    ipv4_t       ipv4;
    icmp_t       icmp;
    tcp_t        tcp;
    hula_t       hula;
    udp_t        udp;
}
#include "includes/fwd.p4"
#include "includes/incoming.p4"
#include "includes/port_counters.p4"
#include "includes/port_knocking.p4"
#include "includes/protected.p4"
/*************************************************************************
*********************** P A R S E R  ***********************************
*************************************************************************/

parser MyParser(packet_in packet,
                out headers hdr,
                inout metadata meta,
                inout standard_metadata_t standard_metadata) {

    state start {
        transition parse_ethernet;
    }

    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            TYPE_IPV4: parse_ipv4;
            default: accept;
        }
    }

    state parse_ipv4 {
        packet.extract(hdr.ipv4);
        transition select(hdr.ipv4.protocol) {
          PROTO_ICMP: parse_icmp;
          PROTO_HULA: parse_hula;
          PROTO_TCP: parse_tcp;
          default: accept;
        }
    }


    state parse_icmp {
        packet.extract(hdr.icmp);
        transition accept;
    }

    state parse_hula {
        packet.extract(hdr.hula);
        transition accept;
    }

    state parse_tcp {
        packet.extract(hdr.tcp);
        transition accept;
    }

}

/*************************************************************************
************   C H E C K S U M    V E R I F I C A T I O N   *************
*************************************************************************/

control MyVerifyChecksum(inout headers hdr, inout metadata meta) {
    apply {  }
}


/*************************************************************************
**************  I N G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyIngress(inout headers hdr,
                  inout metadata meta,
                  inout standard_metadata_t standard_metadata) {

    register<bit<1>>((1)) myclone_reg;

    /* Port-knocking firewall components */
    bit<1> myclone;
    bit<8> curr_reg_header = 0;
    Port_counters_ingress() port_counters_ingress;

    /* My counter decals */
    // counter(10,CounterType.bytes) my_port_counter;

    /* My register used */
    register<PacketByteCount_t>((255)) my_port_reg;

    /****** Registers to keep track of utilization. *******/
    // These can be used for arguing keeping states
    // Keep track of the port utilization
    register<util_t>((255)) port_util;
    // Last time port_util was updated for a port.
    register<time_t>((255)) port_util_last_updated;
    // Keep track of the last time a probe from dst_tor came.
    register<time_t>((255)) update_time;
    // Best hop for for each tor
    register<port_id_t>((255)) best_hop;
    // Last time a packet from a flowlet was observed.
    register<time_t>((255)) flowlet_time;
    // The next hop a flow should take.
    register<port_id_t>((255)) flowlet_hop;
    // Keep track of the minimum utilized path
    register<util_t>((255)) min_path_util;

    /****** HULA Components end ******/

    action drop() {
        mark_to_drop(standard_metadata);
    }

    /****** Port Knocking Components Start  ******/

    register<pk_stage_t>((460798)) pk_reg;

    /****** Port Knocking Components End    ******/

    action ipv4_forward(macAddr_t dstAddr, egressSpec_t port) {
        standard_metadata.egress_spec = port;
        hdr.ethernet.srcAddr = hdr.ethernet.dstAddr;
        hdr.ethernet.dstAddr = dstAddr;
        hdr.ipv4.ttl = hdr.ipv4.ttl - 1;
    }

    table fwd_tb {
        key = {
            hdr.ipv4.dstAddr : lpm;
        }
        actions = {
            ipv4_forward;
            drop;
            NoAction;
        }
        default_action = drop;
    }



    /******************************************************/


    /**** Core HULA logic *****/

    action hula_handle_probe() {
        time_t curr_time = standard_metadata.ingress_global_timestamp;
        bit<32> dst_tor = (bit<32>) hdr.hula.dst_tor;

        util_t tx_util;
        util_t mpu;
        time_t up_time;

        port_util.read(tx_util, (bit<32>) standard_metadata.ingress_port);
        min_path_util.read(mpu, dst_tor);
        update_time.read(up_time, dst_tor);

        // If the current link util is higher, then that is the path util.
        if(hdr.hula.path_util < tx_util) {
            hdr.hula.path_util = tx_util;
        }

        // If the path util from probe is lower than minimum path util,
        // update best hop.
        bool cond = (hdr.hula.path_util < mpu || curr_time - up_time > KEEP_ALIVE_THRESH);

        mpu = cond ? hdr.hula.path_util : mpu;

        min_path_util.write(dst_tor, mpu);

        up_time = cond ? curr_time : up_time;

        update_time.write(dst_tor, up_time);

        port_id_t bh_temp;
        best_hop.read(bh_temp, dst_tor);
        bh_temp = cond ? standard_metadata.ingress_port : bh_temp;

        best_hop.write(dst_tor, bh_temp);


        min_path_util.read(mpu, dst_tor);
        hdr.hula.path_util = mpu;
    }

    action send_to_port(inout standard_metadata_t act_meta,
                    in port_id_t egress_port)
    {
        act_meta.egress_port = egress_port;
    }


    action hula_handle_data_packet() {
        time_t curr_time = standard_metadata.ingress_global_timestamp;
        bit<32> dst_tor = (bit<32>) hdr.hula.dst_tor;

        util_t tx_util;
        port_util.read(tx_util, (bit<32>) standard_metadata.ingress_port);

        bit<32> flow_hash;
        time_t flow_t;
        port_id_t flow_h;
        port_id_t best_h;

        hash(flow_hash, HashAlgorithm.csum16, 32w0, {
            hdr.ipv4.srcAddr,
            hdr.ipv4.dstAddr,
            hdr.ipv4.protocol,
            hdr.tcp.srcPort,
            hdr.tcp.dstPort
        }, 32w1 << 10 - 1);

        flowlet_time.read(flow_t, flow_hash);

        best_hop.read(best_h, meta.dst_tor);
        port_id_t tmp;
        flowlet_hop.read(tmp, flow_hash);
        tmp = (curr_time - flow_t > FLOWLET_TOUT) ? best_h : tmp;
        flowlet_hop.write(flow_hash, tmp);


        flowlet_hop.read(flow_h, flow_hash);
        standard_metadata.egress_spec = flow_h;
        flowlet_time.write(flow_hash, curr_time);
    }

    table hula_logic {
        key = {
          hdr.ipv4.protocol: exact;
        }
        actions = {
          hula_handle_probe;
          hula_handle_data_packet;
          drop;
          NoAction;
        }
        size = 4;
        default_action = NoAction();
    }

    action not_allowed(){
        meta.pk_metadata.allowed = false;
        drop();
    }

    action allow_pkt(){
        meta.pk_metadata.allowed = true;
    }

    table protected_service_tb {
        key = {
            meta.pk_metadata.stage : exact;
            hdr.ipv4.dstAddr : lpm;
            // hdr.tcp.dstPort : exact;
        }
        actions = {
            drop;
            allow_pkt;
            not_allowed;
            NoAction;
        }
        default_action = not_allowed;
    }

    /***********************************************/

    /***** Implement mapping from dstAddr to dst_tor ********/
    // Uses the destination address to compute the destination tor and the id of
    // current switch. The table is configured by the control plane.
    action set_dst_tor(tor_id_t dst_tor, tor_id_t self_id) {
        meta.dst_tor = (bit<32>) dst_tor;
        meta.self_id = (bit<32>) self_id;
    }

    // Used when matching a probe packet.
    action dummy_dst_tor() {
        meta.dst_tor = 0;
        meta.self_id = 1;
    }

    table get_dst_tor {
        key= {
          hdr.ipv4.dstAddr: exact;
        }
        actions = {
          set_dst_tor;
          dummy_dst_tor;
        }
        default_action = dummy_dst_tor;
    }

    /***********************/

    /********* Implement forwarding for edge nodes. ********/
    action simple_forward(egressSpec_t port, macAddr_t dstAddr) {
        standard_metadata.egress_spec = port;
        hdr.ethernet.dstAddr = dstAddr;
    }

    table edge_forward {
        key = {
          hdr.ipv4.dstAddr: exact;
        }
        actions = {
          simple_forward;
          drop;
          NoAction;
        }
        size = 10;
        default_action = NoAction();
    }

    /******************************************************/

    action update_ingress_statistics() {
      util_t util;
      time_t last_update;

      time_t curr_time = standard_metadata.ingress_global_timestamp;
      bit<32> port= (bit<32>) standard_metadata.ingress_port;

      port_util.read(util, port);
      port_util_last_updated.read(last_update, port);

      bit<8> delta_t = (bit<8>) (curr_time - last_update);
      util = (((bit<8>) standard_metadata.packet_length + util) << PROBE_FREQ_FACTOR) - delta_t;
      util = util >> PROBE_FREQ_FACTOR;

      port_util.write(port, util);
      port_util_last_updated.write(port, curr_time);

    }
    

    action not_from_server() {
        meta.pk_metadata.direct_fwd = false;
    }

    action direct_forward() {
        meta.pk_metadata.direct_fwd = true;
    }

    table port_tb {
        key = {
            standard_metadata.ingress_port : exact;
        }
        actions = {
            direct_forward;
            not_from_server;
            drop;
            NoAction;
        }
        default_action = not_from_server;
    }

    action reset_stage(){
        meta.pk_metadata.stage = 0;
    }

    action modify_stage(){
        meta.pk_metadata.stage = meta.pk_metadata.stage + 1;
    }


    table port_knocking_tb {
        key = {
            meta.pk_metadata.stage : exact;
            hdr.ipv4.dstAddr : lpm;
            hdr.ipv4.protocol : exact;
            hdr.tcp.dstPort: exact;
        }
        actions = {
            modify_stage;
            reset_stage;
            NoAction;
        }
        default_action = reset_stage;
    }

    action no_id(){
        meta.pk_metadata.has_id = false;
    }

    action id_found(id_t current_id){
        meta.pk_metadata.id = current_id;
        meta.pk_metadata.has_id = true;
    }

    // Table to get an ID from a source IP address

    table ip_2_id_tb {
        key = {
            hdr.ipv4.srcAddr : lpm;
        }
        actions = {
            id_found;
            no_id;
        }
        default_action = no_id;
    }

    action store_info_into_meta_1(bit<8> reg_num, bit<24> index_num, bit<80> value) { 
        meta.pkIn_metadata_1.reg_num = reg_num; 
        meta.pkIn_metadata_1.index_num = index_num; 
        meta.pkIn_metadata_1.value = value;
        meta.num_state_changes = meta.num_state_changes + 1;
    }


    apply {

        myclone_reg.read(myclone,0);
        port_counters_ingress.apply(hdr, standard_metadata);

        // pk_reg.read(meta.pk_metadata.stage, (bit<32>)meta.pk_metadata.index);

        port_tb.apply(); // original incoming

        meta.pk_metadata.id = (bit<16>)((bit<32>)hdr.ipv4.srcAddr - 167772160); // nice number

        if(meta.pk_metadata.direct_fwd == false){
            pk_reg.read(meta.pk_metadata.stage, (bit<32>)meta.pk_metadata.id);
            protected_service_tb.apply();
        }
        // If the packet is allowed or can be directly forwarded

        if(meta.pk_metadata.allowed == true || meta.pk_metadata.direct_fwd == true){
            
                fwd_tb.apply();

                my_port_reg.read(meta.temp_count, (bit<32>)standard_metadata.ingress_port);
                meta.temp_count = meta.temp_count + (bit<80>)standard_metadata.packet_length;
                my_port_reg.write((bit<32>)standard_metadata.ingress_port, meta.temp_count);
                
                if(myclone == 1 && standard_metadata.ingress_port == 1){
                    store_info_into_meta_1(0,(bit<24>)standard_metadata.ingress_port,(bit<80>)meta.temp_count);
                    clone_preserving_field_list(CloneType.I2E,5,0);
                }
                return;  
        }else{
            if(hdr.tcp.isValid() && hdr.tcp.flags != TCP_SYN){
                drop();
            }else{
                if(hdr.ipv4.isValid() ){
                    //Finally this might be a knock indeed
                    port_knocking_tb.apply();
                    pk_reg.write((bit<32>)meta.pk_metadata.id, meta.pk_metadata.stage);
                    if(myclone == 1){
                        store_info_into_meta_1(8,(bit<24>)meta.pk_metadata.id,(bit<80>)meta.pk_metadata.stage);
                        clone_preserving_field_list(CloneType.I2E,5,0);
                    }
                }
            }
        }

        // if packet not dropped by firewall! do load-balance
        // slightly modified version since we only have one destination
        // the hula exists only for the purpose of stateful components.
        if(standard_metadata.egress_spec != DROP_PORT) {
            //drop();
            //return;
            //get_dst_tor.apply(); 
            update_ingress_statistics();
            if (hdr.ipv4.isValid()) {
                hula_logic.apply();
                if (hdr.hula.isValid()) {
                    standard_metadata.mcast_grp = (bit<16>)standard_metadata.ingress_port;
                }
                // if (meta.dst_tor == meta.self_id) {
                edge_forward.apply();
                // }
            }
        }
    }
}

/*************************************************************************
****************  E G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyEgress(inout headers hdr,
                 inout metadata meta,
                 inout standard_metadata_t standard_metadata) {

      
    action send_to_controller_with_data(bit<8> reg_num, bit<24> index_num, bit<80> value){ 
        hdr.packet_in.setValid(); 
        hdr.packet_in.reg_num = reg_num; 
        hdr.packet_in.index_num = index_num; 
        hdr.packet_in.value = value;
    }
    apply { 
        if(standard_metadata.instance_type == 1){ 
            send_to_controller_with_data(meta.pkIn_metadata_1.reg_num, meta.pkIn_metadata_1.index_num, meta.pkIn_metadata_1.value); 
            truncate(14);
        }else{
            hdr.packet_in.setInvalid();
        }
    }
}

/*************************************************************************
*************   C H E C K S U M    C O M P U T A T I O N   **************
*************************************************************************/

control MyComputeChecksum(inout headers  hdr, inout metadata meta) {
     apply {
        update_checksum(
            hdr.ipv4.isValid(),
            { hdr.ipv4.version,
              hdr.ipv4.ihl,
              hdr.ipv4.diffserv,
              hdr.ipv4.totalLen,
              hdr.ipv4.identification,
              hdr.ipv4.flags,
              hdr.ipv4.fragOffset,
              hdr.ipv4.ttl,
              hdr.ipv4.protocol,
              hdr.ipv4.srcAddr,
              hdr.ipv4.dstAddr },
            hdr.ipv4.hdrChecksum,
            HashAlgorithm.csum16);
    }
}

/*************************************************************************
***********************  D E P A R S E R  *******************************
*************************************************************************/

control MyDeparser(packet_out packet, in headers hdr) {
    apply {
        packet.emit(hdr.packet_in);
        packet.emit(hdr.ethernet);
        packet.emit(hdr.ipv4);
        packet.emit(hdr.icmp);
        packet.emit(hdr.tcp);
        packet.emit(hdr.hula);
    }
}

/*************************************************************************
***********************  S W I T C H  *******************************
*************************************************************************/

V1Switch(
MyParser(),
MyVerifyChecksum(),
MyIngress(),
MyEgress(),
MyComputeChecksum(),
MyDeparser()
) main;
