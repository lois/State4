# plot the received packets

import matplotlib.pyplot as plt
import os

font = {'size'   : 14}

plt.rc('font', **font)
# plt.rc('text', usetex=True)   
plt.rc('pdf',fonttype=42)

savefig = True
x_axis = []
for i in range(0,62):
    x_axis.append(i)

# dir = os.read()

no_reconf_y = []
with open("rpc_no_reconf.txt",'r') as f:
    no_reconf_y = f.read().splitlines()
    no_reconf_y = list(map(int, no_reconf_y))


reconf_control_sync = []
with open("rpc_reconf_control_update.txt",'r') as f:
    reconf_control_sync = f.read().splitlines()
    reconf_control_sync = list(map(int, reconf_control_sync))


reconf_full_sync = []
with open("rpc_reconf_full_update.txt",'r') as f:
    reconf_full_sync = f.read().splitlines()
    reconf_full_sync = list(map(int, reconf_full_sync))

reconf_no_sync = []
with open("rpc_no_update.txt",'r') as f:
    reconf_no_sync = f.read().splitlines()
    reconf_no_sync = list(map(int, reconf_no_sync))

reconf_swing_state = []
with open("rpc_swing_state.txt",'r') as f:
    reconf_swing_state = f.read().splitlines()
    reconf_swing_state = list(map(int, reconf_swing_state))
# plt.rc('text', usetex = True)
# plt.rc('font', family ='serif')

print(len(no_reconf_y))
print(len(reconf_control_sync))
print(len(reconf_full_sync))
print(len(reconf_no_sync))
print(len(reconf_swing_state))

plt.vlines(8, 0, 10, linestyle="dashed",colors='grey')
plt.vlines(19, 0, 41, linestyle="dashed",colors='grey')


plt.plot(x_axis,no_reconf_y,label='No reconfiguration')
plt.plot(x_axis,reconf_no_sync,label='No state preserving')
plt.plot(x_axis,reconf_control_sync,label='Control-plane only')
plt.plot(x_axis,reconf_full_sync, label='State4')
plt.plot(x_axis,reconf_swing_state, label = "Swing State")


plt.xlabel('Time [s]')
plt.ylabel('Received packets count')
plt.grid(axis='x', b=False)
plt.grid(axis='y', b=True)

plt.tight_layout()
plt.legend()
plt.xlim([0,60])

if savefig:
    plt.savefig("state_comp.pdf")
else:
    plt.show()