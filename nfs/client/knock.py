from scapy.all import *
from time import sleep
import random

conf.verb=0

def knock(ports):
    print ("[*] Knocking on ports"+str(ports))
    for dport in ports:
        print(dport)
        ip = IP(dst = "10.0.2.2")
        SYN = TCP(sport=20, dport=dport, flags="S")
        send(ip/SYN)
        sleep(0.5)

# start = time.time()
ports = [10000,20000,30000]
knock(ports)
# end = time.time()
print("Knocked")