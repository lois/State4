

from atexit import register
from nis import match
from operator import truediv
# from typing import SupportsRound
from pkg_resources import to_filename
import sys
import time
from yaml import scan

debug = True

# NOTE: we used a slightly modified version of the simple_switch_CLI
#       to better parse the output 
# behavioral-model/targets/simple_switch/sswitch_CLI.py

sys.path.append("../../p4-guide/bin/behavioral-model/targets/simple_switch/")
sys.path.append("../../p4-guide/bin/behavioral-model/tools/")

import sswitch_CLI
import runtime_CLI
import mycontroller


import socket
import queue
import p4_code_scanner
import sys
import state_saver
import os
import string
import subprocess
# import PreType 
import argparse
import code_instrumenter
without_control_plane_rule_update = 0
without_register_state_update = 1
without_controller_update = 0
from multiprocessing import Process, Queue, Pipe

sys.path.append('../utils/')
sys.path.append('../utils/p4runtime_lib')

import mycontroller


    
def is_hex(x): # need to write the correct way to parse this!
    if('a' in x):
        return True
    return False

def get_parser():
    
    class ActionToPreType(argparse.Action):
        def __init__(self, option_strings, dest, nargs=None, **kwargs):
            if nargs is not None:
                raise ValueError("nargs not allowed")
            super(ActionToPreType, self).__init__(
                option_strings, dest, **kwargs)

        def __call__(self, parser, namespace, values, option_string=None):
            assert(type(values) is str)
            setattr(namespace, self.dest, PreType.from_str(values))

    parser = argparse.ArgumentParser(description='BM runtime CLI')
    # One port == one device !!!! This is not a multidevice CLI
    parser.add_argument('-w','--without-runtime', help='turns on scannaer and instrumentation mode only, program exits after these two',
                        action='store_true', required = False, default = False)
    
    parser.add_argument('--thrift-port', help='Thrift server port for table updates',
                        type=int, action="store", default=9090)

    parser.add_argument('--thrift-ip', help='Thrift IP address for table updates',
                        type=str, action="store", default='localhost')

    parser.add_argument('--json', help='JSON description of P4 program',
                        type=str, action="store", required=False)

    parser.add_argument('--pre', help='Packet Replication Engine used by target',
                        type=str, choices=['None', 'SimplePre', 'SimplePreLAG'],
                        default=runtime_CLI.PreType.SimplePre, action=ActionToPreType)

    parser.add_argument('--state-input-file', dest="state_filename",
                        help="Input file containing a list of state components names to keep",
                        type =str, action="store", default=None, required=False)
    return parser

def run_runtime_cli(args):
    services = runtime_CLI.RuntimeAPI.get_thrift_services(args.pre)
    services.extend(sswitch_CLI.SimpleSwitchAPI.get_thrift_services())

    standard_client, mc_client,sswitch_client = runtime_CLI.thrift_connect(
            args.thrift_ip, args.thrift_port, services
    )
    print("retrieved args.json is:")
    print(args.json)
    print("**********************")


    runtime_CLI.load_json_config(standard_client, args.json)

    sswitch_CLI.SimpleSwitchAPI(args.pre, standard_client, mc_client,sswitch_client).cmdloop()


# use data from scanner and put each command into msg_queue
def read_states(scanner,msg_queue):
    print("--- %s seconds for scanner and instrumenter ---" % (time.time() - start))

    # table entries would not be directly changed by P4 program
    for table_name in scanner.table_names:
        msg_queue.put("table_dump " + table_name + " \n")
        # set block_reg to be 1
        msg_queue.put("register_write block_reg 0 1 \n")
        msg_queue.put("mirroring_add 5 255 \n") # mirroring the session to CPU port

    for reg in scanner.register_names:

        msg_queue.put("register_read " + reg + " \n")

    return 0 # normal finish

def parse_state(args):
    
    temp_out_filename = "out.txt"
    null_file = '/dev/null'
    sys.stdout = open(temp_out_filename,"w")
    run_runtime_cli(args)
    pass


# going through the list to see if its component matches any in the component_list
def check_if_in(component,component_list):
    for item in component_list:
        if(component == item):
            return True
    return False


def child_read_and_add_rule(r, w, start, scanner):

    os.close(r) # close read side for scanner
    w = os.fdopen(w, "w")

    print("--- %s seconds for scanner and instrumenter ---" % (time.time() - start))

    # table entries would not be directly changed by P4 program
    for table_name in scanner.table_names:
        w.write("table_dump " + table_name + " \n")
    # set clone_reg to be 1
    w.write("register_write myclone_reg 0 1 \n")
    w.write("mirroring_add 5 255 \n") # mirroring the session to CPU port
    for reg in scanner.register_names:
        w.write("register_read " + reg + " \n")
    


def parse_table(temp_out_filename,scanner, retrived_states):
    start = time.time()
    f = open(temp_out_filename, "r")
    while f:
        line = f.readline() 
        if(line == ""):
            break
        # parse regsiters
        for reg_name in scanner.register_names:
            pos = line.find(reg_name)
            if(pos != -1 ):
                line = line.replace(" ","").replace("\n","") # clean up white space and \n
                pos = line.find(reg_name)
                if(pos != -1):
                    states = line[pos+len(reg_name)+1:]
                    states = states.split(',')
                    retrived_states.registers.add_register(reg_name,states)

        pos = line.find("TABLE NAME:")

        # parse table entries
        if(pos != -1):
            table_name = line[pos+len("TABLE NAME:")+1:-1].replace(" ","")
            retrived_states.tables.append(retrived_states.table_components(table_name))
            f.readline() #  line: =======

            # continuous read until end of table and construct the table entry
            if f.readline() != "TABLE ENTRIES\n":
                sys.stderr("ERROR HERE\n")
            while True:
                # TODO: make into function
                if f.readline() == "==========\n":
                    break
                line = f.readline() # Dumping Entry xxx
                entry_index = line[line.find("0x"):-1]
                line = f.readline() # match key:
                match_field = ""
                while True: 
                    line = f.readline() # key match-kind match-field
                    if(line.startswith("Action entry:")):
                        break

                    words = line.split(" ")
                    # Word is break into match_key match_kind and match_field
                    words = list(filter(None, words))
                    
                    if(len(words) < 5):
                        if(words[0] == 'Dumping'): break

                    field = words[-1].replace("\n","")
                    if(is_hex(field) or field[0]!= '0'):
                        field = "0x" + field
                    else: 
                        field = field.lstrip('0')
                        if field == "":
                            field = '0'
                    match_field = match_field + (field + " ")


                skip_len = len("Action entry: ")
                cursor = line.find("-",skip_len+1)
                action_name = line[skip_len: cursor].replace(" ","")
                params = line[cursor+1:].replace(" ","")
                retrived_states.tables[-1].add_entry(action_name, match_field, params)

        pos = line.find("Dumping default entry")
        if(pos != -1):
            pass
    print("--- %s seconds for parsing switch states ---" % (time.time() - start))

def push_states(r,w, retrived_states, scanner,queue):
    os.close(r)
    sys.stdout = os.fdopen(w,"w")

    start = time.time()

    #load_new_config_file path_to_config
    sys.stderr.write("INFO: load_new_config_file ./nfs/build/switch.json")

    print("load_new_config_file ./nfs/build/switch.json")
    if not without_register_state_update:
        for reg_name in retrived_states.registers.reg_names:
            if(reg_name == 'clone_reg'): continue
            reg_array = retrived_states.registers.registers[reg_name]
            for reg in reg_array: # reg is of register class
                if(reg.value != "0"): # NOTE: no waste to write zero value, i.e. uninitualized in our case
                    sys.stderr.write("INFO: register_write " + reg_name +" " + str(reg) + "\n")
                    print("register_write " + reg_name +" " + str(reg))

    if not without_control_plane_rule_update:
        sys.stderr.write("Retrieved table size = {}\n".format(len(retrived_states.tables)))
        for table in retrived_states.tables:
            # sys.stderr.write("number of entries in the table")
            for table_entry in table.entries:
                sys.stderr.write("table_add MyIngress." + table.table_name + " " + str(table_entry) + "\n")
                print("table_add " + table.table_name + " " + str(table_entry))

    # Here need to drain reg_info_queue
    reg_name = ""
    index_num = 0
    value_num = 0
    write_entries = 0

    if not without_controller_update and not queue.empty():
        curr = queue.get(block=False) # TODO: handle queue of size 1.
        reg_infos = curr.split()
        print("reg_info",reg_infos)
        reg_name = scanner.register_names[int(reg_infos[0])] 
        index_num = reg_infos[1]
        value_num = reg_infos[2]
        
        while True:

            if(queue.empty()):
                # reg_infos = curr.split()
                # reg_name = scanner.register_names[int(reg_name)] 
                # index_num = reg_infos[1]
                # value_num = reg_infos[2]
                sys.__stderr__.write("INFO: register_write " + reg_name +" " +index_num + " "+ value_num + "\n")
                print("register_write " + reg_name +" " +index_num + " "+ value_num)
                write_entries = write_entries + 1
                break
            next = queue.get(block=False)
            print(next)
            if(next):
                next_reg_infos = next.split()
                next_reg_name = scanner.register_names[int(next_reg_infos[0])] 
                next_index_num = reg_infos[1]
                if(next_reg_name == reg_name and next_index_num == index_num):
                    reg_name = next_reg_name
                    index_num = next_index_num
                    value_num = next_reg_infos[2] 
                    continue                    
    
            sys.__stderr__.write("INFO: register_write " + reg_name +" " +index_num + " "+ value_num + "\n")
            print("register_write " + reg_name +" " +index_num + " "+ value_num)
            write_entries = write_entries + 1
            # curr = next

    sys.__stderr__.write("INFO: swap_configs\n")
    print("swap_configs")
    sys.__stderr__.write("--- %s seconds for sending information ---\n" % (time.time() - start))
    
    # sys.__stderr__.write("INFO: Last write = register_write " + reg_name +" " +index_num + " "+ value_num + "\n")

    # Then continue to read to compensate for the delta-update between last read and swap_configs.
    # count = 0
    # time.sleep(1)
    
    sys.__stderr__.write("Re-synced num_entry = {}\n".format(write_entries))
    # sys.__stderr__.write("Missing num_entry = {}\n".format(count))


def main():
    args = get_parser().parse_args()
    retrived_states = state_saver.states()
    reg_info_queue = Queue()
    print(args.state_filename)
    # statefile specified

    state_components_to_keeps = [] # can identify the components manually
    
    # preprocessing
    
    if(args.state_filename is not None):
        with open(args.state_filename,"r") as f:
            for line in f.readlines():
                state_components_to_keeps.append(line.replace("\n","").replace())


    start = time.time()

    scanner = p4_code_scanner.codeScanner("nfs/switch.p4")
    print("MAX INDEX:",scanner.max_index_count)
    print("reg_info:",scanner.reg_write_infos)
    instrumenter = code_instrumenter.codeInstrumenter(scanner.fName ,scanner, scanner.keywordLines,scanner.codes)
    scanner.is_instrumented = True
    
    if(not scanner.is_instrumented):
        instrumenter.instrument()
    print(len(scanner.register_names))
    print(scanner.register_names)

    if(args.without_runtime):
        sys.exit(1)
    print("INFO: scanner and instrumentor done!")


    p_controler = Process(target=mycontroller.main,args=("listener_data.txt",reg_info_queue))
    p_controler.start()    
    time.sleep(1) # NOTE: needed to handle correct controller startup
    r,w = os.pipe()

    p_read_add_rule = Process(target=child_read_and_add_rule, args=(r, w, start,scanner))
    p_read_add_rule.start()

    # Processsing input output redirects and pipes
    # Parent needs to close the pipe here
    os.close(w)
    temp_out_filename = "out.txt"

    sys.stdout = open(temp_out_filename,"w")
    sys.stdin = os.fdopen(r,"r")
    sys.stderr.write("****** runtime_cli started ******\n")
    start = time.time()
    run_runtime_cli(args)

    # Debug code for using profiler 
    # import cProfile, pstats, io
    # from pstats import SortKey
    # with cProfile.Profile() as pr:
    # sys.stdout = open('runtime_cli_profiler.log','w')
    # p = pstats.Stats(pr)
    # p.sort_stats(SortKey.TIME).print_stats()

    sys.stdout = sys.__stdout__
    print("--- %s seconds for simple_switch_CLI to read states ---" % (time.time() - start))
    print("****** runtime_cli exited ******")

    # the read_add_rule process should be finished now and needs to reap
    p_read_add_rule.join()


    parse_table(temp_out_filename, scanner, retrived_states)
  

    r, w = os.pipe()
    p_push_states = Process(target=push_states, args=(r, w, retrived_states, scanner, reg_info_queue))
    p_push_states.start()



    os.close(w)
    sys.stdin = os.fdopen(r,"r") # read from this
    start = time.time()

    sys.stdout = open('/dev/null','w')
    run_runtime_cli(args)
    
    sys.stdout = sys.__stdout__
    print("--- %s seconds for switch to sync ---" % (time.time() - start))
    p_push_states.join()

    # controller should be safe to kill since run_time_cli finished 
    p_controler.kill()
    p_controler.join()

if __name__ == "__main__":
    main()
