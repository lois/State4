from atexit import register
from nis import match
from sqlite3 import register_converter
from enum import Enum
import sys




class states:
    def __init__(self):
        self.registers = self.register_components()
        self.counters = self.counter_components()
        self.meters = self.meter_components()
        self.tables = [] # a list of table components

    def __str__(self):
            return str(self.registers) 
    
    class table_components:


        def __init__(self,table_name):
            self.table_name = table_name
            self.entries = []
            self.num_entries = 0
            # self.match_kind = None

        def add_entry(self, action_name, match_fields, action_params, priority = 0):

            self.entries.append(self.table_entry(action_name,match_fields,action_params))                
            self.num_entries += 1

        def add_default_entry(self, action_name):
            pass


        class table_entry:
            
            def __init__(self, action_name, match_fields, action_params, isDefault=False):
                self.isDefault = isDefault
                self.table_action = self.action(action_name,match_fields,action_params)
                
            def __str__(self):
                return str(self.table_action)

            class action:

                def __init__(self, action_name, match_fields, action_params):
                    self.action_name = action_name
                    self.match_fields = match_fields
                    if isinstance(action_name, str):
                        action_params = action_params.replace(" ","")
                        params = action_params.split(",")
                        self.action_params = params # use a string format for now

                def __str__(self):
                    str = self.action_name + " " +self.match_fields + " => "
                    for param in self.action_params:
                        if not param.replace(" ","").replace("\n","") == "":
                            str += ("0x" + param+ " ")
                    return str 

            

    class register_components: 
        
        class regsiter:
            def __init__(self, index, value):
                self.index = index
                self.value = value
            
            def __str__(self):
                return str(self.index) + " " + str(self.value) + " "

     
        def __str__(self):
            reg_str = ""
            for reg_array in self.reg_names:
                reg_str += (reg_array+": ")
                for reg in self.registers[reg_array]:
                    reg_str += str(reg)

            return reg_str

        def __init__(self):
            self.registers = {}
            self.num_registers = 0
            self.reg_names = []

        def add_register(self, register_name, register_data_array):
            self.registers[register_name] = []
            self.reg_names.append(register_name)
            index = 0 #inits to 0
            for value in register_data_array:
                self.registers[register_name].append(self.regsiter(index, value))
                index = index + 1
            self.num_registers = self.num_registers + 1


    class counter_components:
        class counter:
            
            def __init__(self,name,index, packets, bytes):
                self.name = name
                self.index = index 
                self.packets = packets
                self.bytes = bytes

            def __str__(self):
                return self.name + " " + self.index + " " + self.packets + " " + self.bytes
        
        def __init__(self):
            self.counters = []
            self.num_counters = 0

            
    class meter_components:
        class meters:
            pass
        def __init__(self):
            pass

