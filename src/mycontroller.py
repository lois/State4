#!/usr/bin/env python2
import argparse
import grpc
import os
import sys
import pdb
from time import sleep
import queue
from random import randrange



# Import P4Runtime lib from parent utils dir
# Probably there's a better way of doing this.
sys.path.append(
    os.path.join(os.path.dirname(os.path.abspath(__file__)),
                 '../utils/'))
sys.path.append(
    os.path.join(os.path.dirname(os.path.abspath(__file__)),
                 '../utils/p4runtime_lib/'))
sys.path.append(
    os.path.join(os.path.dirname(os.path.abspath(__file__)),
                 '../utils/mininet/'))
import p4runtime_lib.bmv2
from p4runtime_lib.switch import ShutdownAllSwitchConnections
import p4runtime_lib.helper
from p4.v1 import p4runtime_pb2


def printGrpcError(e):
    print ("gRPC Error:", e.details()),
    status_code = e.code()
    print ("(%s)" % status_code.name),
    traceback = sys.exc_info()[2]
    print ("[%s:%d]" % (traceback.tb_frame.f_code.co_filename, traceback.tb_lineno))


# converts a list of byte value from a unicode string back to its integer
def convert_to_int(list, base):
    i = len(list) - 1
    total = 0
    for val in list:
        print(val)
        print(base)
        total += (val * (base**i))
        i = i -1
    return total
# inspired by p4runtime_shell from https://github.com/p4lang/p4runtime-shell
def ListenCPUPort(s1,fName,queue):

    # init
    num_msg = 0
    reg_count = 0
    index_num = 0
    value_num = 0
    sys.stderr.write("Listener Online\n")


    while True:
        msg = s1.getStreamPacket('packet')
        if not msg:
            print("Finished receiving packets from P4Runtime Server.")
            break

        reg_count = int.from_bytes(msg.packet.metadata[0].value,byteorder='big')
        index_num = int.from_bytes(msg.packet.metadata[1].value,byteorder='big')
        value_num = int.from_bytes(msg.packet.metadata[2].value, byteorder='big') 

        # debug code
        # print("reg_count,index_num,value = " + str(reg_count) +"," + str(index_num) +","+ str(value_num)+ "\n")
     
        num_msg = num_msg + 1

        # optimization here for detecting if a value has been changed
        if(int(value_num) != 0): 
            queue.put(str(reg_count) +" " + str(index_num) +" "+ str(value_num))

            
    
def parse_args():
    parser = argparse.ArgumentParser(description='P4Runtime Controller')
    parser.add_argument('--p4info', help='p4info proto in text format from p4c',
                        type=str, action="store", required=False,
                        default='./nfs/build/switch.p4.p4info.txt')
    parser.add_argument('--bmv2-json', help='BMv2 JSON file from p4c',
                        type=str, action="store", required=False,
                        default='./nfs/build/switch.json')
    parser.add_argument('--forwarding-action', help='Set the action for SetForwardingPipelineConfig RPC',
                        type=str, action='store', required=False)
                        # comment for precaution in case of bug
                        # default='VERIFY_AND_COMMIT')
    parser.add_argument('--listen-cpu', help = 'decides to whether listen to the cpu port of the switch',
                        action='store_true',required=False)
    parser.add_argument('--register-bit-size', help='Represents the size used for storing register index',
                        type=int, action='store',required=False)

    args = parser.parse_args()

    # bypass the args even if forgotten to add the argument
    args.listen_cpu = True
    if args.listen_cpu:
        return args
    if not os.path.exists(args.p4info):
        parser.print_help()
        print ("\np4info file not found: %s\nHave you run 'make'?" % args.p4info)
        parser.exit(1)
    if not os.path.exists(args.bmv2_json):
        parser.print_help()
        print ("\nBMv2 JSON file not found: %s\nHave you run 'make'?" % args.bmv2_json)
        parser.exit(1)
    if not (args.forwarding_action == "VERIFY_AND_COMMIT" or \
            args.forwarding_action == "VERIFY_AND_SAVE" or \
            args.forwarding_action == "COMMIT"):
        parser.print_help()
        print("Unsupported SetForwardingPipelineConfig Action: ", args.forwarding_action)
        parser.exit(1)

    return args


def main(fName,queue):

    try:
        # Create a switch connection object for s1
        # this is backed by a P4Runtime gRPC connection.
        # Also, dump all P4Runtime messages sent to switch to given txt files.
        s1 = p4runtime_lib.bmv2.Bmv2SwitchConnection(
            name='s1',
            address='127.0.0.1:50051',
            device_id=0,
            proto_dump_file='s1-logs.txt')

        s1.MasterArbitrationUpdate()
        print("master connected?")
        # if args.listen_cpu:
        ListenCPUPort(s1,fName,queue)

        return 0

    except KeyboardInterrupt:
        print (" Shutting down.")
    except grpc.RpcError as e:
        printGrpcError(e)

    ShutdownAllSwitchConnections()

if __name__ == '__main__':
    #
    fName = "listener_data.txt"
    from multiprocessing import Queue
    q = Queue()
    main(fName, q)

