# consider use modifier

import code
from distutils.log import debug
import pdb
from re import I
# from locale import currency
# from msvcrt import LK_LOCK
# from signal import SIG_DFL
# from ssl import SSLSocket
import sys
import enum
import math
from numpy import isin
from setuptools import find_namespace_packages


class component_type(enum.Enum): # three types of components
    register = 1
    counter = 2
    meter = 3

# object to store register data
class reg_write_info:
    def __init__(self, reg_index, index_var_name, value_var_name):
        self.reg_num = reg_index
        self.index_var_name = index_var_name
        self.value_var_name = value_var_name

    def __str__(self):
        return self.reg_num + " " + self.index_var_name + " " + self.value_var_name 



# Tokenizer parses the original file into a tree
class tokenizer:
    def __init__(self, line, debug=False):
        pass

# The scanner auto scan the given file when intialized.
class codeScanner:

    # internal object to store the information about the location of the keyword.
    class keywordLine:
        def __init__(self, lineStr, lineNum):
            self.line = lineStr
            self.lineNum = lineNum
            
        def __str__(self):
            return "line:" + str(self.lineNum) + " " + self.line


    def __init__(self, fName, debug=False):
        self.debug = debug
        if(self.debug):
            print("******* DEBUG MODE ON *******")
        if type(fName) is not str:
            print("Scanner Type-check Failed: Input filename has to be string")
        self.fName = fName


        # init
        # self.block_comment = False
        self.reg_write_infos = {}
        self.num_stateful_components = 0 # temporarily only count components in the Ingress
        self.max_index_count = 0
        self.is_instrumented = False 
        self.currentLineNum = 1 
        self.register_names = []
        self.counter_names = []
        self.meter_names = []
        self.table_names = []        
        self.keywordLines = []
        self.ingress_line_num = 0 # 0 not correct
        self.egress_line_num = 0
        self.ingress_apply_block_start_line_num = 0 #
        self.const_map = {}
        self.typedef_map = {}
        self.egress_apply_block_start_line_num = 0
        self.egress_apply_block_end_line_num = 0
        self.header_insert_line_num = 0
        self.metadata_insert_line_num = 0
        self.inside_ingress = False # internal state for scanner module
        self.inside_egress = False
        self.state_components_bit_size = 0
        self.max_index_bit_size = 0   
        self.max_index_value = 0 

        # clone reg used as indicator to identify reconfiguration process
        self.block_reg_name = "clone_reg"


        self.codes = self.parse_file()

    
        if(self.debug):
            print("PRINTING REGISTERS")
            for reg in self.register_names:
                print(reg)
            print("PRINTING COUNTERS")
            for reg in self.counter_names:
                print(reg)
            print("PRINTING METERS")
            for reg in self.meter_names:
                    print(reg)



    def find_component_in(self, line, container):
        # do sanity check for the container type?
        i = 0
        for component in container:
            if(line.find(component) != -1):
                return component,i
            i+=1
        return None,0    


        
    def add_stateful_components(self, component_name, type):
        if(self.debug and (not isinstance(type,component_type))):
            print("Not supported component type {}".format(type))
        if(type == component_type.register):
            self.register_names.append(component_name)
        if(type == component_type.counter):
            self.counter_names.append(component_name)
        if(type == component_type.meter):
            self.meter_names.append(component_name)
        self.num_stateful_components += 1

    # source https://stackoverflow.com/questions/1883980/find-the-nth-occurrence-of-substring-in-a-string
    def find_nth(self,str,obj,n):
        start = str.find(obj)
        while(start > 0 and n > 1):
            start = str.find(obj,start+1)
            n -= 1
        return start


    def get_component_name(self, line):
        result = None
        i = 0
        result,i = self.find_component_in(line, self.register_names)
        if(result is not None): return result,i
        result,i = self.find_component_in(line, self.counter_names)
        if(result is not None): return result,i
        result,i = self.find_component_in(line, self.meter_names)
        return result,i
        
            
    # parses each line according to the content
    # curr_line_num is used as context for the line to know which part
    #       of the code this line belongs to. 
    def parse_line(self, line, curr_line_num):
        line = line.lstrip() # trim leading white space

        #skipping comments
        if line.startswith("//"): 
            return


        # store const into associated map for future reference
        if(line.startswith("const ")):
            words = line.split(" ")

            if(words[4].find("w")):
                return
            val = words[4].replace(";","").replace("\n","")
            try:
                val = int(val)
            except ValueError:
                val = int(val,16)
            self.const_map[words[2]] = val 

        # record the width for each typedef
        if(line.startswith("typedef")):
            words = line.split(" ")
            temp_val= line[line.find("<")+1:line.find(">")]
            size = int(temp_val)
            self.typedef_map[words[2].replace(";","").replace("\n","")] = size 


        # find location to insert header
        if(self.inside_ingress and line.find("myclone_reg")):
            self.is_instrumented = True

        # insert based starts from the line before
        if(line.startswith("struct header")):
            self.header_insert_line_num  = curr_line_num - 1
        if(line.startswith("struct metadata")):
            self.metadata_insert_line_num = curr_line_num - 1

        # checking stateful component
        if(line.startswith("table")):
            table_name = line[line.find("table")+ len("table"):-2].replace(" ","")
            if table_name not in self.table_names:
                self.table_names.append(table_name) # table is not runtime stateful components
                self.keywordLines.append(self.keywordLine(line,curr_line_num))


        # register decal also needs to find out the max possible value for index        
        if(line.startswith("register") and self.inside_ingress):
            # known issue, doesn't support #define and const for defining the index count
            # probably better when this module is integrated into the p4c after preprocessing
            reg_name_loc = line.find(")",line.find(")")+1)+1
            index_count = line[self.find_nth(line, '(', 2)+1:self.find_nth(line, ')',1)]

            index_size = line[line.find("<")+1:line.find(">")]
            reg_name = line[reg_name_loc:-2].replace(" ","") # also truncate white space
            
            if(reg_name == "myclone_reg"): # do nothing 
                return
            
            # compute the index_size to use as 
            if(not index_size.isnumeric()): # not number probably definied as tyepdef size
                index_size = self.typedef_map[index_size]
            index_size = int(index_size)
            if(index_size > self.max_index_value):
                self.max_index_value = index_size
            if(int(index_count) > self.max_index_count):
                self.max_index_count = int(index_count)

            if(reg_name not in self.register_names):
                self.add_stateful_components(reg_name, component_type.register)
                self.keywordLines.append(self.keywordLine(line, curr_line_num))
        

        if(line.startswith("counter") and self.inside_ingress):
            counter_name = line[line.find(")")+1:-2].replace(" ","")
            if( counter_name  not in self.counter_names):
                self.add_stateful_components(counter_name, component_type.counter)
                self.keywordLines.append(self.keywordLine(line, curr_line_num))
        # TODO: add meter implementation here
        
        # finding Ingress 
        if(line.startswith("MyIngress") and self.ingress_line_num == 0):
            self.ingress_line_num = curr_line_num
            self.inside_ingress = True

        if(line.startswith("apply") and self.inside_ingress):
            self.ingress_apply_block_start_line_num = curr_line_num
            self.inside_ingress = False

        # finding egress
        if(line.startswith("MyEgress") and self.egress_line_num == 0):
            self.egress_line_num = curr_line_num
            self.inside_egress = True

        if(line.startswith("apply")  and self.inside_egress):
            self.egress_apply_block_start_line_num = curr_line_num
            self.inside_egress = False


        # if the line refers to a defined component
        component_name,reg_num = self.get_component_name(line)
        if component_name is None: 
            return
        if(self.debug):
            print("component ", component_name ,"found: ", line )
        if(line.find(component_name+".write") != -1):
            if(self.debug):
                print ("Find write access, line is:" , self.keywordLine(line,curr_line_num))
            self.keywordLines.append(self.keywordLine(line,curr_line_num))
            params = line[line.find("(")+1:len(line)-2]
            params = params.split(",") #params =[index,value]

            index_var_name = params[0].replace(" ","")
            if(index_var_name.find(")")!= -1):
                index_var_name = index_var_name[index_var_name.find(")")+1:]
            value_var_name = params[1][:-1].replace(" ","")
            self.reg_write_infos[component_name] = reg_write_info(reg_num,index_var_name,value_var_name)
            



    # source https://www.geeksforgeeks.org/round-to-next-greater-multiple-of-8/
    def RoundUp(self,val):
        return ((val+7) & (-8))

    # parse input file once to retrieve names of all stateful components
    def parse_file(self):
        codes = [] #init
        with open(self.fName,"r") as f:
            curr_line_num = 0
            codes = f.readlines()
            for line in codes:
                # make into function
                self.parse_line(line, curr_line_num)
                curr_line_num = curr_line_num + 1
                # if(self.debug):
                #     print(curr_line_num," " ,line)


        self.state_components_bit_size = math.ceil(math.log(self.num_stateful_components,2))
        self.max_index_bit_size = math.ceil(math.log(self.max_index_count,2))
        self.state_components_bit_size = self.RoundUp(self.state_components_bit_size)
        self.max_index_bit_size = self.RoundUp(self.max_index_bit_size)
        print("Tables read are: ")
        print(self.table_names)
        print("Number of stateful components = {}".format(self.num_stateful_components))
        print("Max_index_count = {}".format(self.max_index_count))
        print("Max_index_value = {}".format(self.max_index_value))
        print("Size needed for stateful components = {}".format(self.state_components_bit_size))
        print("Size needed for max index = {}".format(self.max_index_bit_size))
        # insanity check!
        if(self.ingress_line_num == 0
            or self.ingress_apply_block_start_line_num == 0
            ):
            print("Error: Insanity check failed, ingress_line_num or ingress_apply_block_line_num cannot be 0!!!")
            return 
        
        return codes


    def insert_block_regsiter(self):
        return self.block_reg_name # ??? doesn't seem correct

    def get_register_names(self):
        return self.register_names

    def get_counter_names(self):
        #counter_names = []
        # counter_lines
        return self.counter_names

    def get_meter_names(self):
        pass


    def parsekeywordLines(self):
        pass


# test code for code scanner
if __name__ == '__main__':
    if( len(sys.argv) < 2 ):
        print("Usage: python p4_code_scanner filename")
        sys.exit(1)
    scanner = codeScanner(sys.argv[1])
    #scanner.parse_file()
    # for line in scanner.keywordLines:
    #     print(line)