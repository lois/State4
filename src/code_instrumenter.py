
# make this code modifier?
class codeInstrumenter:

    def __init__(self, fName, scanner, keywordLines,codes, debug=False):
        self.current_line_shifted = 0
        # sanity check needed?
        self.fName = fName
        self.keywordLines = keywordLines
        self.codes = codes
        self.debug = debug
        self.scanner = scanner
        
        
    # Only use this function to instrument the code, to maintain the correct shift value
    def insert_code(self, index, code):
        self.codes.insert(index+ self.current_line_shifted, code+"\n")
        self.current_line_shifted += 1

    def instrument(self):
        
        # counter = 4
        if(self.debug):
            # for line in code:
            print("ingress_line_num = ", self.scanner.ingress_line_num)
            print("ingress_apply_block_start_line_num = ", self.scanner.ingress_apply_block_start_line_num)
            print("ingress_apply_block_end_line_num = " , self.scanner.ingress_apply_block_end_line_num)
            print("header_insert_line_num = ", self.scanner.struct)
            print("metadata_insert_line_num = ", self.scanner.metadata_insert_line_num)
        
        
        self.insert_code(self.scanner.metadata_insert_line_num - 1, 
            "struct packetIn_metadata{{ bit<{}> reg_num; bit<{}> index_num; bit<{}> value;}}".format(self.scanner.state_components_bit_size, self.scanner.max_index_bit_size,self.scanner.max_index_value))
        self.insert_code(self.scanner.metadata_insert_line_num + 2,
            "   @field_list(0)\n\tpacketIn_metadata pkIn_metadata;")
        self.insert_code(self.scanner.header_insert_line_num -1,
            "@controller_header(\"packet_in\")")
        self.insert_code(self.scanner.header_insert_line_num -1,
            "header packet_in_header_t{{ bit<{}> reg_num; bit<{}> index_num; bit<{}> value;}}".format(self.scanner.state_components_bit_size, self.scanner.max_index_bit_size,self.scanner.max_index_value))
        self.insert_code(self.scanner.header_insert_line_num+2,"packet_in_header_t packet_in;")
        self.insert_code(self.scanner.ingress_line_num + 4, "register<bit<1>>((1)) block_reg;")
        self.insert_code(self.scanner.ingress_line_num + 5, "bit<1> block;");


        self.insert_code(self.scanner.ingress_apply_block_start_line_num - 1,
            "action store_info_into_meta(bit<{}> reg_num, bit<{}> index_num, bit<{}> value) {{ meta.pkIn_metadata.reg_num = reg_num; meta.pkIn_metadata.index_num = index_num; meta.pkIn_metadata.value = value;}}".format(
                self.scanner.state_components_bit_size, self.scanner.max_index_bit_size,self.scanner.max_index_value
            ))

        self.insert_code(self.scanner.ingress_apply_block_start_line_num + 1 ,"block_reg.read(block,0);")

        # During Egress, add action
        self.insert_code(self.scanner.egress_line_num + 1, 
            "action send_to_controller_with_data(bit<{}> reg_num, bit<{}> index_num, bit<{}> value){{ hdr.packet_in.reg_num = reg_num; hdr.packet_in.index_num = index_num; hdr.packet_in.value = value;}}".format(
                self.scanner.state_components_bit_size, self.scanner.max_index_bit_size,self.scanner.max_index_value
            ))

        # During Egress, add check for clone and set packet_header and truncate  
        self.insert_code(self.scanner.egress_apply_block_start_line_num+1,
            "if(standard_metadata.instance_type == 1){{ hdr.packet_in.setValid(); send_to_controller_with_data(meta.pkIn_metadata.reg_num, meta.pkIn_metadata.index_num, meta.pkIn_metadata.value); truncate({});}}".format(
                (int(self.scanner.state_components_bit_size + self.scanner.max_index_bit_size+ self.scanner.max_index_value) / 8)
            ))        

        # use keywordlines to instrument the code
        # TODO: insert the code a bit differently inside a if statement
        for keywordLine in self.keywordLines:
            if(self.debug): print(keywordLine)
            # for now only supports registers
            if(keywordLine.line.find(".write") != -1): # write access to state components 
                reg_name = ""

                loc = keywordLine.line.find(".write")    
                if(loc != -1):
                    reg_name = keywordLine.line[:loc].replace(" ","")
                infos = self.scanner.reg_write_infos[reg_name]
                insert_loc = keywordLine.lineNum -1
                self.insert_code(insert_loc,"if(block == 1){")
                self.insert_code(insert_loc,"store_info_into_meta({},{},{});".format(
                    infos.reg_num, infos.index_var_name, infos.value_var_name
                ));
                self.insert_code(insert_loc,"clone_preserving_field_list(CloneType.I2E,5,0);")
                self.insert_code(insert_loc, "}")

        
        # write the output to the file
        with open(self.fName,"w") as f:
            codes = "".join(self.codes)
            f.write(codes)
        # return codes